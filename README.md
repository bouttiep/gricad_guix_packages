# Package Guix pour les utilisateurs de GRICAD

Le but de ce dépôt est de fournir de façon ad-hoc des paquets guix pour leurs besoins en calcul et en traitement de données. Si les paquets semblent stables et fonctionnels, [charge aux mainteneurs de les proposer en upstream](https://guix.gnu.org/manual/en/html_node/Contributing.html)). 

## Comment accéder aux paquets de ce dépôt ?

Quand vous utiliser guix, l'ensemble des paquets disponibles proviennent [du channel guix par défaut.](https://hpc.guix.info/browse)

**WARNING :** Pour utiliser `guix` sur les clusters, faites toujours `source /applis/site/guix-start.sh`  sur la frontale ou dans vos scripts de soumission.

Pour ajouter ce channel précis et donc pouvoir installer les paquets à l'aide de la commande `guix install`, il faut créer un fichier `~/.config/guix/channels.scm` qui contiendra les lignes suivantes : 

```
;; Add gricad packages to those Guix provides.
(cons (channel
        (name 'gricad-guix-packages)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git"))
      %default-channels)
```

Une fois ceci fait, lancez la commande `guix pull`. 

Après, le `guix pull`, comme indiqué en sortie, exécutez les commandes suivantes, pour être sûr d'utiliser la commande `guix` à jour : 

```
GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"
```


Pour vérifier que vous avez bien accès aux paquets gricad, vous pouvez tenter l'installation du paquet `hello-gricad` :

```
guix install hello-gricad
```

## Comment spécifier plusieurs channels ?

Si vous avez déjà ajouté un channel à votre session guix (ou si vous souhaitez ajouter un autre channel en plus de celui de Gricad), vous pouvez modifier votre fichier `~/.config/guix/channels.scm` comme ceci:

```
;; Add gricad packages to those Guix provides.
(cons (channel
        (name 'gricad-guix-packages)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git"))
      (channel
        (name 'other-guix-packages)
        (url "https://other/url/guix-packages.git"))
      %default-channels)
```