(define-module (common python-fluidfft)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system gnu)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (common python-packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages check)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages commencement)
  #:use-module (common python-fluiddyn))


(define-public python-transonic
  (package
    (name "python-transonic")
    (version "0.6.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "transonic" version))
       (sha256
        (base32 "05565z2w8zyx1qf13k8mqdn5px4wfcg1nmc31f77wya5d8jmw4vc"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-autopep8 
              python-beniget 
              python-gast
              python-mpi4py 
              python-numpy
              python-pdm-backend))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check phase
              (delete 'check))))
    (home-page "https://transonic.readthedocs.io")
    (synopsis "Make your Python code fly at transonic speeds!")
    (description "Transonic is a pure Python package (requiring Python >= 3.9) to easily accelerate 
      modern Python-Numpy code with different accelerators (currently Cython, Pythran, Numba and JAX, 
      but potentially later Cupy, PyTorch, Weld, Pyccel, etc...).")
    (license license:bsd-3)))

(define-public pfft
  (package
  (name "pfft")
  (version "1.0.8-alpha")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/mpip/pfft")
              (commit "e4cfcf9902d0ef82cb49ec722040932b6b598c71")))
        (file-name (git-file-name name version))
        (sha256 (base32 "02q3hdwwj5bw4ylnq0a5kvvd7iavqhl0k89sbsg7xb94hphrm4kz"))))
    (inputs 
        (list fftw
              fftw-openmpi
              libtool
              autoconf
              automake
              openmpi))
    (build-system gnu-build-system)
    (home-page "https://github.com/mpip/pfft")
    (synopsis "Parallel fast Fourier transforms ")
    (description "PFFT is a software library for computing massively parallel, fast Fourier transformations 
      on distributed memory architectures. PFFT can be understood as a generalization of FFTW-MPI to multidimensional 
      data decomposition. The library is written in C and MPI. A Fortran interface is also available. Support for 
      hybrid parallelization based on OpenMP and MPI is under development.")
    (license license:gpl3+)))

(define-public python-fluidfft-pfft
  (package
    (name "python-fluidfft-pfft")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidfft_pfft" version))
       (sha256
        (base32 "1ia383kgps4m3gfvpc55l3gywmyl46r44mbi27xxpln9g2ymb0pg"))))
    (build-system pyproject-build-system)
    (inputs 
        (list python-fluidfft-builder
              meson-python
              python-transonic
              pkg-config
              fftw
              fftw-openmpi
              openmpi
              pfft))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Add a phase to set path of PFFT input package
              ; (add-before 'configure 'add_PFFT_PATH
              ;     (lambda _
              ;         (setenv "PFFT_DIR" (assoc-ref %build-inputs "pfft"))))
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check)
          )))
    (home-page "https://foss.heptapod.net/fluiddyn/fluidfft")
    (synopsis "Fluidfft plugin for MPI FFTs using fftw")
    (description "This plugin provides a method for parallel FFTs using PFFT: fft3d.mpi_with_pfft.")
    (license license:gpl2)))

(define-public p3dfft
  (package
  (name "p3dfft")
  (version "2.7.9")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/sdsc/p3dfft")
              (commit "3110dea6a9235b557d9a8e90c2f47ea4f3c2acb1")))
        (file-name (git-file-name name version))
        (sha256 (base32 "16jzg40n2qw59majr8rg26pmykc2ka36siwvbdm06wd25p72dvfd"))))
    (inputs (list fftw
                  fftw-openmpi
                  openmpi
                  gfortran-toolchain
                  automake
                  autoconf
                  libtool
                  python2-minimal))
    (arguments '(#:parallel-build? #f
                 #:phases (modify-phases %standard-phases
                   ;; Add a phase to fix /bin/sh interpreter
                   (add-after 'unpack 'fix_binsh
                      (lambda* (#:key inputs #:allow-other-keys)
                          (for-each (lambda (f)
                            (invoke "sed" "-i" (string-append "s@/bin/sh@" (which "sh") "@g") f))
                                      (find-files "."))))
                   ;; Add a phase to fix version of aclocal and automake
                   (add-before 'build 'fix_versions
                     (lambda _
                       (invoke "sed" "-i" "s@aclocal-1.14@aclocal-1.16@g" "Makefile")
                       (invoke "sed" "-i" "s@automake-1.14@automake-1.16@g" "Makefile")
                       ))
                   ;; Replace configure phase
                   (replace 'configure
                     (lambda _
                       (invoke "./configure" "--enable-openmpi" "--enable-gnu" "--enable-fftw" 
                        (string-append "--with-fftw=" (assoc-ref %build-inputs "fftw"))
                        (string-append "--prefix=" (getenv "out")) 
                        "CC=mpicc" (string-append "LDFLAGS=-lgfortran -lm")
                        "FCFLAGS=-w -fallow-argument-mismatch -O2 -fPIC")
                       ))
                   )))
    (build-system gnu-build-system)
    (home-page "https://p3dfft.readthedocs.io/en/latest/home.html")
    (synopsis "P3DFFT - Parallel Three-Dimensional Fast Fourier Transforms.")
    (description "P3DFFT is a library for large-scale computer simulations on parallel platforms. 
      3D FFT is an important algorithm for simulations in a wide range of fields, including studies 
      of turbulence, climatology, astrophysics and material science. This project was initiated at 
      San Diego Supercomputer Center (SDSC) at UC San Diego by its main author Dmitry Pekurovsky, Ph.D.")
    (license license:gpl3+)))

(define-public python-fluidfft-p3dfft
  (package
    (name "python-fluidfft-p3dfft")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidfft_p3dfft" version))
       (sha256
        (base32 "09zqybcy3if93fx13y6rwb90bx65fy15cpkjp7wbji9hlhv14a8w"))))
    (build-system pyproject-build-system)
    (inputs 
        (list python-fluidfft-builder
              meson-python
              python-transonic
              pkg-config
              gfortran-toolchain
              fftw
              fftw-openmpi
              openmpi
              p3dfft))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Add a phase to set path of P3DFFT input package
              ;; and add link to openmpi and gfortran libraries for compiling
              (add-before 'build 'add_PFFT_PATH
                  (lambda _
                      (setenv "P3DFFT_DIR" (assoc-ref %build-inputs "p3dfft"))
                      (invoke "sed" "-i" (string-append "s@'-lfftw3_mpi'@'-lfftw3_mpi', '-lmpi_mpifh', '-lgfortran', '" (assoc-ref %build-inputs "gfortran-toolchain") "/lib/libgfortran.a'@g") "meson.build")))
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check)
          )))
    (home-page "https://foss.heptapod.net/fluiddyn/fluidfft")
    (synopsis "Fluidfft plugin for MPI FFTs using fftw")
    (description "This plugin provides a method for parallel FFTs using P3DFFT: fft3d.mpi_with_p3dfft.")
    (license license:gpl2)))

(define-public python-fluidfft-mpi-with-fftw
  (package
    (name "python-fluidfft-mpi-with-fftw")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidfft_mpi_with_fftw" version))
       (sha256
        (base32 "1kpnhwvh1yjwgfn059zm22k6g52hx44r836kz295hwa5wxkii35b"))))
    (build-system pyproject-build-system)
    (inputs 
        (list python-fluidfft-builder
              meson-python
              python-transonic
              pkg-config
              fftw
              openmpi))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check)
          )))
    (home-page "https://foss.heptapod.net/fluiddyn/fluidfft")
    (synopsis "Fluidfft plugin for MPI FFTs using fftw")
    (description "This plugin provides two methods for parallel FFTs using FFTW3: fft2d.mpi_with_fftw1d 
      and fft3d.mpi_with_fftw1d")
    (license license:gpl2)))

(define-public python-fluidfft-fftwmpi
  (package
    (name "python-fluidfft-fftwmpi")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidfft_fftwmpi" version))
       (sha256
        (base32 "1lv0yqdfyj6zxsk6cw9sf65i7fcsqrc7akzang0w54g9a9l60g5g"))))
    (build-system pyproject-build-system)
    (inputs 
        (list python-fluidfft-builder
              meson-python
              python-transonic
              pkg-config
              fftw
              fftw-openmpi
              openmpi))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check)
          )))
    (home-page "https://foss.heptapod.net/fluiddyn/fluidfft")
    (synopsis "Fluidfft plugin for MPI FFTs using fftw_mpi")
    (description "This plugin provides two methods for parallel FFTs using FFTW3: fft2d.mpi_with_fftwmpi2d 
      and fft3d.mpi_with_fftwmpi3d.")
    (license license:gpl2)))

(define-public python-fluidfft-fftw
  (package
    (name "python-fluidfft-fftw")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidfft_fftw" version))
       (sha256
        (base32 "1zykjqxr3s4fc14mh2cwxhjc1rn1x63mkxhgnf6m0xnrw537i5jr"))))
    (build-system pyproject-build-system)
    (inputs 
        (list pkg-config
              meson-python
              python-transonic
              fftw
              python-fluidfft-builder))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check)
          )))
    (home-page "https://foss.heptapod.net/fluiddyn/fluidfft")
    (synopsis "Fluidfft plugin using fftw")
    (description "This plugin provides three methods for sequential FFTs using FFTW3: fft2d.with_fftw1d, 
      fft2d.with_fftw2d and fft3d.with_fftw3d.")
    (license license:gpl2)))

(define-public python-fluidfft-builder
  (package
    (name "python-fluidfft-builder")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidfft_builder" version))
       (sha256
        (base32 "0vvbvf6rrzv08zjigiyqzdr3wvgkaxmmxiflqsczffgjl7nim6ds"))))
    (build-system pyproject-build-system)
    (inputs (list python-flit-core))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check phase
              (delete 'check)
          )))
    (home-page "https://foss.heptapod.net/fluiddyn/fluidfft")
    (synopsis "Fluidfft plugin dependencies")
    (description "C++ source, Cython templates and build utilities for Fluidfft. 
      Used as a build dependency for plugins using C++.")
    (license license:gpl2)))

(define-public python-fluidfft
  (package
    (name "python-fluidfft")
    (version "0.4.0.post1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidfft" version))
       (sha256
        (base32 "0kfbhyy4y60vdim1aj19w2dwlpw78pknkl49vg2i2xixyj91qybh"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-dask
              python-fluiddyn
              python-fluidfft-fftw
              python-fluidfft-fftwmpi
              python-fluidfft-mpi-with-fftw
              python-fluidfft-p3dfft
              python-fluidfft-pfft
              python-importlib-metadata
              python-mpi4py
              python-pyfftw
              python-transonic
              meson-python
              pkg-config
              cmake))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ; Add a phase to set $HOME environment variable
              (add-before 'build 'patch-HOME-path
                  (lambda _
                      (setenv "HOME" (getenv "out"))))
              ;; Add a phase to install all the tests
              (add-after 'install 'install-test
                  (lambda* (#:key outputs inputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (spdir (string-append out "/lib/python3.10/site-packages/fluidfft/tests")))
                             ;; move contents of git clone directory to install directory
                             (copy-recursively "tests/" spdir))))
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check)
          )))
    (home-page "https://foss.heptapod.net/fluiddyn/fluidfft")
    (synopsis "Efficient and easy Fast Fourier Transform (FFT) for Python.")
    (description "Fluidfft provides C++ classes and their Python wrapper classes written in Cython useful 
      to perform Fast Fourier Transform (FFT) with different libraries, in particular : fftw3 and fftw3-mpi, 
      pfft, p3dfft, mpi4py-fft, cufft (fft library by CUDA running on GPU)")
    (license license:gpl2)))
