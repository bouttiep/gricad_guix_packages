(define-module (common gudhi)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix utils)
      #:use-module (guix build utils)
      #:use-module (guix build-system cmake)
      #:use-module (gnu packages)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages boost)
      #:use-module (gnu packages xml)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages python))

(define-public gudhi
  (package
    (name "gudhi")
    (version "3.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/GUDHI/gudhi-devel/releases/download/tags%2Fgudhi-release-" version "/gudhi." version ".tar.gz"))
        (sha256
          (base32
            "0nibfvvwr0l14yd8kmry55l129hyq5fsrjsz77rpnzag0yjmgvj3"))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags
        (list "-DCMAKE_BUILD_TYPE=Release"
  	          "-DWITH_GUDHI_EXAMPLE=ON"
  	          (string-append "-DCMAKE_INSTALL_PREFIX="
  			                     (assoc-ref %outputs "out")))
       #:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'fix-and-disable-failing-tests)
          (delete 'sanity-check)
          (replace 'configure
            (lambda* (#:key inputs outputs configure-flags #:allow-other-keys)
  	          (mkdir-p "./build")
  	          (chdir "./build")
                    (apply invoke "cmake" ".." configure-flags))))))
    (inputs
      `(("python" ,python-wrapper)
        ("boost" ,boost)))
    (home-page "https://github.com/GUHDI/gudhi-devel")
    (synopsis
      "The GUDHI library is a generic open source C++ library, with a Python interface, for Topological Data Analysis (TDA) and Higher Dimensional Geometry Understanding.")
    (description
      "The GUDHI library is a generic open source C++ library, with a Python interface, for Topological Data Analysis (TDA) and Higher Dimensional Geometry Understanding. The library offers state-of-the-art data structures and algorithms to construct simplicial complexes and compute persistent homology.")
    (license license:bsd-4)))
