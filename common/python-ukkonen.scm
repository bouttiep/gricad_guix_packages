(define-module (common python-ukkonen)
   #:use-module  ((guix licenses) #:prefix license:)
   #:use-module  (gnu packages)
   #:use-module  (guix packages)
   #:use-module  (guix download)
   #:use-module  (guix build-system python)
   #:use-module  (guix build-system pyproject)
   #:use-module  (gnu packages python)
   #:use-module  (gnu packages python-build)
   #:use-module  (gnu packages libffi))


(define-public python-ukkonen
  (package
    (name "python-ukkonen")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ukkonen" version))
       (sha256
        (base32 "1yp0m3y1s97sfd9bbi9k9jynn8s6ddgmls8qqfkrkjy9j6lxjslp"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-cffi))
    (native-inputs (list python-setuptools python-wheel))
    (home-page "https://www.github.com/asottile/ukkonen")
    (synopsis "Implementation of bounded Levenshtein distance (Ukkonen)")
    (description "Implementation of bounded Levenshtein distance (Ukkonen).")
    (license license:expat)))
