(define-module (common r-packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages java)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages python)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system r)
  #:use-module (gnu packages compression)
  #:use-module (guix build-system gnu))

(define-public r-h2o-custom
  (package
    (name "r-h2o-custom")
    (version "3.42.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "h2o" version))
       (sha256
        (base32 "16wjcaskwkjw5bn4mj73qsywd9qcp8q906p3sfsr1vdk9qgm7bls"))))
    (properties `((upstream-name . "h2o")))
    (build-system r-build-system)
     (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-before 'install 'ugly-trick-jar
            (lambda* (#:key inputs #:allow-other-keys)
              (let* ((java-dir (string-append #$output "/java"))
                     (jar (string-append java-dir "/h2o.jar")))
                (mkdir-p java-dir)
                (copy-file
                 (assoc-ref inputs "h2o.jar")
                 jar)
                (setenv "H2O_JAR_PATH" jar)))))))
    (search-paths
     (list (search-path-specification
            (variable "H2O_JAR_PATH")
            (file-type 'regular)
            (separator #f)
            (files (list "java/h2o.jar")))))
    (native-search-paths search-paths)
    (inputs `(("h2o.jar"
               ,(origin
                  (method url-fetch)
                  (uri "https://s3.amazonaws.com/h2o-release/h2o/rel-3.42.0/2/Rjar/h2o.jar")
                  (sha256
                   (base32 "11z95h2spfwahi1pgrw5l2vgfl30dggrqdbf89l9g47allzknxxi"))))))
    (propagated-inputs (list openjdk
                             r-codetools
                             r-jsonlite
                             r-rcurl))
    (home-page "https://github.com/h2oai/h2o-3")
    (synopsis "R Interface for the 'H2O' Scalable Machine Learning Platform")
    (description
     "R interface for H2O', the scalable open source machine learning platform that
offers parallelized implementations of many supervised and unsupervised machine
learning algorithms such as Generalized Linear Models (GLM), Gradient Boosting
Machines (including XGBoost), Random Forests, Deep Neural Networks (Deep
Learning), Stacked Ensembles, Naive Bayes, Generalized Additive Models (GAM),
ANOVA GLM, Cox Proportional Hazards, K-Means, PCA, @code{ModelSelection},
Word2Vec, as well as a fully automatic machine learning algorithm (H2O
@code{AutoML}).")
    (license license:asl2.0)))

(define-public r-janitor
  (package
    (name "r-janitor")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "janitor" version))
        (sha256
          (base32
            "09nqm957m2f54y2l30619b58x4i7gxwvr2lwg5kly5xy1ya1a1nn"))))
    (properties `((upstream-name . "janitor")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-dplyr" ,r-dplyr)
        ("r-lifecycle" ,r-lifecycle)
        ("r-lubridate" ,r-lubridate)
        ("r-magrittr" ,r-magrittr)
        ("r-purrr" ,r-purrr)
        ("r-rlang" ,r-rlang)
        ("r-snakecase" ,r-snakecase)
        ("r-stringi" ,r-stringi)
        ("r-stringr" ,r-stringr)
        ("r-tidyr" ,r-tidyr)
        ("r-tidyselect" ,r-tidyselect)))
    (native-inputs `(("r-knitr" ,r-knitr)))
    (home-page "https://github.com/sfirke/janitor")
    (synopsis
      "Simple Tools for Examining and Cleaning Dirty Data")
    (description
      "The main janitor functions can: perfectly format data.frame column names; provide quick counts of variable combinations (i.e., frequency tables and crosstabs); and isolate duplicate records.  Other janitor functions nicely format the tabulation results.  These tabulate-and-report functions approximate popular features of SPSS and Microsoft Excel.  This package follows the principles of the \"tidyverse\" and works well with the pipe function %>%.  janitor was built with beginning-to-intermediate R users in mind and is optimized for user-friendliness.  Advanced R users can already do everything covered here, but with janitor they can do it faster and save their thinking for the fun stuff.")
    (license license:expat)))

(define-public r-audio
  (package
    (name "r-audio")
    (version "0.1-7")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "audio" version))
        (sha256
          (base32
            "0x4wcl8nlymmzhsdd255ybxdwnshksmr92y6drcajnij8mx3kq2j"))))
    (properties `((upstream-name . "audio")))
    (build-system r-build-system)
    (native-inputs `(("pkg-config" ,pkg-config)))
    (home-page "http://www.rforge.net/audio/")
    (synopsis "Audio Interface for R")
    (description
      "Interfaces to audio devices (mainly sample-based) from R to allow recording and playback of audio.  Built-in devices include Windows MM, Mac OS X AudioUnits and PortAudio (the last one is very experimental).")
    (license license:expat)))

(define-public r-beepr
  (package
    (name "r-beepr")
    (version "1.3")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "beepr" version))
        (sha256
          (base32
            "061sfld23b516jws4llml0a4jsdk4z74rll4z58l2rvahkqsdrfp"))))
    (properties `((upstream-name . "beepr")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-audio" ,r-audio) ("r-stringr" ,r-stringr)))
    (home-page
      "https://cran.r-project.org/package=beepr")
    (synopsis
      "Easily Play Notification Sounds on any Platform")
    (description
      "The main function of this package is beep(), with the purpose to make it easy to play notification sounds on whatever platform you are on.  It is intended to be useful, for example, if you are running a long analysis in the background and want to know when it is ready.")
    (license license:gpl3)))

(define-public r-dcpo
  (package
    (name "r-dcpo")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "DCPO" version))
        (sha256
          (base32
            "1h14xnii6kv6sc7pfxalif3bj0mifnxg46x4lwbixx9vzxxz96sq"))))
    (properties `((upstream-name . "DCPO")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-beepr" ,r-beepr)
        ("r-bh" ,r-bh)
        ("r-dplyr" ,r-dplyr)
        ("r-forcats" ,r-forcats)
        ("r-janitor" ,r-janitor)
        ("r-purrr" ,r-purrr)
        ("r-rcpp" ,r-rcpp)
        ("r-rcppeigen" ,r-rcppeigen)
        ("r-rstan" ,r-rstan)
        ("r-rstantools" ,r-rstantools)
        ("r-stanheaders" ,r-stanheaders)
        ("r-tibble" ,r-tibble)
        ("r-tidyr" ,r-tidyr)))
    (home-page
      "https://cran.r-project.org/package=DCPO")
    (synopsis "Dynamic Comparative Public Opinion")
    (description
      "Estimates latent variables of public opinion cross-nationally and over time from sparse and incomparable survey data.  'DCPO' uses a population-level graded response model with country-specific item bias terms.  Sampling is conducted with 'Stan'.  References: Solt (2020) <doi:10.31235/osf.io/d5n9p>.")
    (license license:gpl3+)))

(define-public r-pinfsc50
  (package
    (name "r-pinfsc50")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "pinfsc50" version))
        (sha256
          (base32
            "1547xyxmfb7zi8h9bsm6k67dcw4hpp129xzvmgwfw7r6p4af47zd"))))
    (properties `((upstream-name . "pinfsc50")))
    (build-system r-build-system)
    (home-page
      "https://cran.r-project.org/package=pinfsc50")
    (synopsis
      "Sequence ('FASTA'), Annotation ('GFF') and Variants ('VCF') for 17 Samples of 'P. Infestans\" and 1 'P. Mirabilis'")
    (description
      "Genomic data for the plant pathogen \"Phytophthora infestans.\" It includes a variant file ('VCF'), a sequence file ('FASTA') and an annotation file ('GFF').  This package is intended to be used as example data for packages that work with genomic data.")
    (license (list license:gpl2+ license:gpl3+))))

(define-public r-memuse
  (package
    (name "r-memuse")
    (version "4.1-0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "memuse" version))
        (sha256
          (base32
            "1bbjp8y0ji71956fbaxiil7ynq2nkmmgz7i9xps83m3bbp5d3mjq"))))
    (properties `((upstream-name . "memuse")))
    (build-system r-build-system)
    (home-page
      "https://github.com/shinra-dev/memuse")
    (synopsis "Memory Estimation Utilities")
    (description
      "How much ram do you need to store a 100,000 by 100,000 matrix? How much ram is your current R session using? How much ram do you even have? Learn the scintillating answer to these and many more such questions with the 'memuse' package.")
    (license #f)))

(define-public r-gjam
  (package
    (name "r-gjam")
    (version "2.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "gjam" version))
        (sha256
          (base32
            "1jzgh5hp112gjgqyyhpd4fv1yizb56596wl3izb066rzhhyc4wim"))))
    (properties `((upstream-name . "gjam")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-mass" ,r-mass)
        ("r-rann" ,r-rann)
        ("r-rcpp" ,r-rcpp)
        ("r-rcpparmadillo" ,r-rcpparmadillo)))
    (native-inputs `(("r-knitr" ,r-knitr)))
    (home-page
      "https://cran.r-project.org/package=gjam")
    (synopsis "Generalized Joint Attribute Modeling")
    (description
      "Analyzes joint attribute data (e.g., species abundance) that are combinations of continuous and discrete data with Gibbs sampling.  Full model and computation details are described in Clark et al. (2018) <doi:10.1002/ecm.1241>.")
    (license license:gpl2+)))

(define-public r-vcfr
  (package
    (name "r-vcfr")
    (version "1.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "vcfR" version))
        (sha256
          (base32
            "0lhxb3ac4fafwik9q3cds46svzf0hyca8k54chw3dpk50c0zz1yx"))))
    (properties `((upstream-name . "vcfR")))
    (build-system r-build-system)
    (inputs `(("zlib" ,zlib)))
    (propagated-inputs
      `(("r-ape" ,r-ape)
        ("r-dplyr" ,r-dplyr)
        ("r-magrittr" ,r-magrittr)
        ("r-memuse" ,r-memuse)
        ("r-pinfsc50" ,r-pinfsc50)
        ("r-rcpp" ,r-rcpp)
        ("r-stringr" ,r-stringr)
        ("r-tibble" ,r-tibble)
        ("r-vegan" ,r-vegan)
        ("r-viridislite" ,r-viridislite)))
    (native-inputs `(("r-knitr" ,r-knitr)))
    (home-page "https://github.com/knausb/vcfR")
    (synopsis "Manipulate and Visualize VCF Data")
    (description
      "Facilitates easy manipulation of variant call format (VCF) data.  Functions are provided to rapidly read from and write to VCF files.  Once VCF data is read into R a parser function extracts matrices of data.  This information can then be used for quality control or other purposes.  Additional functions provide visualization of genomic data.  Once processing is complete data may be written to a VCF file (*.vcf.gz).  It also may be converted into other popular R objects (e.g., genlight, DNAbin).  VcfR provides a link between VCF data and familiar R software.")
    (license license:gpl3+)))

(define-public r-startmrca
  (let ((commit
          "9807c18dc5c9ade6dbc0e1cc86cd3a9de9b43a5c")
        (revision "1"))
    (package
      (name "r-startmrca")
      (version (git-version "0.6-1" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/jhavsmith/startmrca")
                 (commit commit)))
          (file-name (git-file-name name version))
          (sha256
            (base32
              "0szy6iv8l9r69ph0cww0nz9mr02d5i30fvnx49zvwqmpl99m69s9"))))
      (properties `((upstream-name . "startmrca")))
      (build-system r-build-system)
      (arguments
      `(#:tests? #f ; No tests implemented.
        #:phases
          (modify-phases %standard-phases
            (add-before
              'patch-source-shebangs 'remove-broken-symlinks
              (lambda _
              ;; These files avoid R to install this package
            (delete-file "src/cprobback.o")
            (delete-file "src/startmrca.so"))))))
      (propagated-inputs `(("r-vcfr" ,r-vcfr)))
      (home-page
        "https://github.com/jhavsmith/startmrca")
      (synopsis
        "Estimating time to the common ancestor for a beneficial allele")
      (description
        "This package uses carriers and non-carriers of a beneficial mutation to estimate the time to the common ancester.")
      (license #f))))

(define-public r-clustmixtype
  (package
    (name "r-clustmixtype")
    (version "0.2-15")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "clustMixType" version))
        (sha256
          (base32 "1m43xhqc7jcdj1h5nlp0daz99s1nfss7rwwjn7qqf1hvscblyp3s"))))
    (properties `((upstream-name . "clustMixType")))
    (build-system r-build-system)
    (propagated-inputs (list r-rcolorbrewer r-tibble))
    (home-page "https://cran.r-project.org/package=clustMixType")
    (synopsis "k-Prototypes Clustering for Mixed Variable-Type Data")
    (description
      "This package provides functions to perform k-prototypes partitioning clustering
for mixed variable-type data according to Z.Huang (1998): Extensions to the
k-Means Algorithm for Clustering Large Data Sets with Categorical Variables,
Data Mining and Knowledge Discovery 2, 283-304, <DOI:10.1023/A:1009769707641>.")
    (license license:gpl2+)))

(define-public r-betapart
  (package
    (name "r-betapart")
    (version "1.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "betapart" version))
        (sha256
          (base32 "1m2rykch2vhh90s08s9s2kmmrxhp9ik32s5zkdw8991cbw2j7n68"))))
    (properties `((upstream-name . "betapart")))
    (build-system r-build-system)
    (propagated-inputs
      (list r-ape
            r-dosnow
            r-fastmatch
            r-foreach
            r-geometry
            r-itertools
            r-picante
            r-rcdd
            r-snow))
    (home-page "https://cran.r-project.org/package=betapart")
    (synopsis
      "Partitioning Beta Diversity into Turnover and Nestedness Components")
    (description
      "This package provides functions to compute pair-wise dissimilarities (distance
matrices) and multiple-site dissimilarities, separating the turnover and
nestedness-resultant components of taxonomic (incidence and abundance based),
functional and phylogenetic beta diversity.")
    (license license:gpl2+)))

(define-public r-phyloregion
  (package
    (name "r-phyloregion")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "phyloregion" version))
        (sha256
          (base32 "161pjy66vhpjwc4h9q4jiqpyzn0v4nawx8ahspjpc7a376rk5552"))))
    (properties `((upstream-name . "phyloregion")))
    (build-system r-build-system)
    (propagated-inputs
      (list r-ape
            r-betapart
            r-clustmixtype
            r-colorspace
            r-dismo
            r-igraph
            r-matrix
            r-phangorn
            r-randomforest
            r-raster
            r-rgdal
            r-rgeos
            r-sp
            r-vegan))
    (native-inputs (list r-knitr))
    (home-page "https://github.com/darunabas/phyloregion")
    (synopsis "Biogeographic Regionalization and Macroecology")
    (description
      "Computational infrastructure for biogeography, community ecology, and
biodiversity conservation (Daru et al.  2020) <doi:10.1111/2041-210X.13478>.  It
is based on the methods described in Daru et al. (2020)
<doi:10.1038/s41467-020-15921-6>.  The original conceptual work is described in
Daru et al. (2017) <doi:10.1016/j.tree.2017.08.013> on patterns and processes of
biogeographical regionalization.  Additionally, the package contains fast and
efficient functions to compute more standard conservation measures such as
phylogenetic diversity, phylogenetic endemism, evolutionary distinctiveness and
global endangerment, as well as compositional turnover (e.g., beta diversity).")
    (license license:agpl3+)))


(define-public r-chirps
(package
  (name "r-chirps")
  (version "0.1.4")
  (source
    (origin
      (method url-fetch)
      (uri (cran-uri "chirps" version))
      (sha256
        (base32 "1yj91rvcsfsr2nmvk7qnxwfkbiblm4bdazs2p83g8ax4fwkrp39c"))))
  (properties `((upstream-name . "chirps")))
  (build-system r-build-system)
  (propagated-inputs
    `(("r-httr" ,r-httr)
	("r-jsonlite" ,r-jsonlite)
	("r-sf" ,r-sf)
	("r-terra" ,r-terra)))
  (native-inputs (list r-knitr))
  (home-page "https://docs.ropensci.org/chirps/")
  (synopsis "API Client for CHIRPS and CHIRTS")
  (description
  "API Client for the Climate Hazards Center 'CHIRPS' and 'CHIRTS'.  The 'CHIRPS' data is a quasi-global (50Â°S â\x80\x93 50Â°N) high-resolution (0.05 arc-degrees) rainfall data set, which incorporates satellite imagery and in-situ station data to create gridded rainfall time series for trend analysis and seasonal drought monitoring. 'CHIRTS' is a quasi-global (60Â°S â\x80\x93 70Â°N), high-resolution data set of daily maximum and minimum temperatures.  For more details on 'CHIRPS' and 'CHIRTS' data please visit its official home page <https://www.chc.ucsb.edu/data>.")
  (license license:expat)))

(define-public r-rnaturalearth
  (package
    (name "r-rnaturalearth")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "rnaturalearth" version))
        (sha256
          (base32 "193b31a7n9jhc607mhwxbpx5gr0fpj3qasm9dbi6kcc7vac3ilgm"))))
    (properties `((upstream-name . "rnaturalearth")))
    (build-system r-build-system)
    (propagated-inputs `(("r-sf" ,r-sf) ("r-sp" ,r-sp)))
    (native-inputs `(("r-knitr" ,r-knitr)))
    (home-page "https://github.com/ropenscilabs/rnaturalearth")
    (synopsis "World Map Data from Natural Earth")
    (description
      "Facilitates mapping by making natural earth map data from <http://www.naturalearthdata.com/> more easily available to R users.")
    (license license:expat)))

