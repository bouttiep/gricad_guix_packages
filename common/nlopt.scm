(define-module (common nlopt)
    #:use-module ((guix licenses) #:prefix license:)
    #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages networking))

(define-public nlopt-with-python
  (package
    (name "nlopt-with-python")
    (version "2.7.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/stevengj/nlopt/")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "1xpdza28i8w441fwv6a5f3qk4zi7ys6ws9fx6kr5ny27dfdz6rr1"))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags (list (string-append "-DPython_EXECUTABLE="
                                              (assoc-ref %build-inputs "python")
                                              "/bin/python3"))
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'set-libnlopt-file-name
           (lambda* (#:key outputs #:allow-other-keys)
             ;; Make sure the Scheme module refers to the library by its
             ;; absolute file name.
             (let ((out (assoc-ref outputs "out")))
               (substitute* "src/swig/nlopt-guile.i"
                 (("\"nlopt_guile\"")
                  (format #f "~s"
                          `(format #f "~anlopt_guile"
                                   (if (getenv "NLOPT_UNINSTALLED")
                                       ""
                                       ,(format #f "~a/lib/guile/3.0/extensions/" out))))))
               (setenv "NLOPT_UNINSTALLED" "1")))))))
    (propagated-inputs (list guile-3.0 octave python python-numpy))
    (native-inputs (list pkg-config swig))
    (home-page "http://ab-initio.mit.edu/wiki/")
    (synopsis "Library for nonlinear optimization")
    (description "NLopt is a library for nonlinear optimization, providing a
common interface for a number of different free optimization routines available
online as well as original implementations of various other algorithms.")
    (license license:lgpl2.1+)))