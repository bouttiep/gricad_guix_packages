(define-module (common siesta)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (gnu packages perl)
      #:use-module (guix utils)
      #:use-module (guix git-download)
      #:use-module (gnu packages autotools)
      #:use-module (common python-pyscf)
      #:use-module (common python-amuse)
      #:use-module (guix build utils)
      #:use-module (guix build-system cmake)
      #:use-module (guix build-system gnu)
      #:use-module (gnu packages)
      #:use-module (gnu packages base)
      #:use-module (gnu packages algebra)
      #:use-module (gnu packages image-processing)
      #:use-module (gnu packages image)
      #:use-module (gnu packages graphics)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages mpi)
      #:use-module (gnu packages cmake)
      #:use-module (gnu packages python)
      #:use-module (gnu packages python-xyz)
      #:use-module (gnu packages compression)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages vim)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages video)
      #:use-module (gnu packages llvm)
      #:use-module (gnu packages multiprecision))


(define-public libxc
  (package
    (name "libxc")
    (version "6.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ElectronicStructureLibrary/libxc")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
        (base32 "04nzkw7dlsj8iscdwkj2rnd2120i0mz7f4g7k4h98p7g3wd6nmqy"))
      )
    )
    (build-system cmake-build-system)
    (arguments
      `(#:configure-flags
	(list "-DBUILD_SHARED_LIBS=ON"
	      "-DENABLE_FORTRAN=ON"
	      "-DENABLE_PYTHON=ON")
	#:phases
        (modify-phases %standard-phases
          (delete 'check))))
    (inputs
      (list perl 
	    gfortran-toolchain
	    python))
    (home-page "https://github.com/ElectronicStructureLibrary/libxc")
    (synopsis "Libxc is a library of exchange-correlation functionals for density-functional theory.")
    (description "Libxc is a library of exchange-correlation functionals for density-functional theory. The aim is to provide a portable, well tested and reliable set of exchange and correlation functionals that can be used by a variety of programs.")
    (license license:mpl2.0)
  )
)

(define-public libgridxc
  (package
    (name "libgridxc")
    (version "1.1.0")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://gitlab.com/siesta-project/libraries/libgridxc/-/archive/" version  "/" name "-" version ".tar.gz"))
	      (sha256
		(base32
		  "0sp40wik2chz03j026dfr3hwzq063hll0xs3b7p3h96vlibkx277"))))
    (build-system gnu-build-system)
    (arguments
      `(#:configure-flags
        (list
          (string-append "--prefix=" (assoc-ref %outputs "out"))
	  (string-append "--with-libxc=" (assoc-ref %build-inputs "libxc"))
	  (string-append "--with-mpi=" (assoc-ref %build-inputs "openmpi"))
          "--enable-multiconfig")
	#:phases
	  (modify-phases %standard-phases
            (add-before 'configure 'setenv
	      (lambda _
		(setenv "FC" "mpif90")
		(setenv "CC" "mpicc"))))
	#:make-flags
	  (list
	    "FC=mpif90"
	    "CC=mpicc")
        #:tests? #f
       )
    )
    (inputs
      (list gfortran-toolchain
            libxc
	    libtool
	    autoconf
	    automake
	    openmpi))
    (home-page "https://gitlab.com/siesta-project/libraries/libgridxc")
    (synopsis "LibGridXC provides routines to calculate the exchange and correlation
energy and potential in spherical (i.e. an atom) or periodic systems")
    (description "LibGridXC provides routines to calculate the exchange and correlation
energy and potential in spherical (i.e. an atom) or periodic systems,
using a variety of LDA and GGA functionals, as well as the van der
Waals DFT functionals of Dion et al (PRL 92, 246401 (2004)), Lee et al
(PRB 82, 081101 (2010)), Klimes et al (JPCM 22, 022201 (2009)), and
Vydrov and VanVoorhis (JCP 133, 244103 (2010)) implemented as
described by Roman-Perez and Soler (PRL 103, 096102 (2009))")
    (license license:expat)))


(define-public xmlf90
  (package
    (name "xmlf90")
    (version "1.5.6")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://gitlab.com/siesta-project/libraries/xmlf90/-/archive/" version "/" name "-" version ".tar.gz"))
	      (sha256
		(base32
		  "0a05s85687mmykibj50hqhxyysphacsaah7f9ywnidhir6b2ajf4"))))
    (build-system gnu-build-system)
    (arguments
      `(#:configure-flags
	(list (string-append "--prefix=" (assoc-ref %outputs "out")))
	#:phases
        (modify-phases %standard-phases
          (delete 'check))))
    (inputs
      (list perl 
	    libtool
	    autoconf
	    automake
	    gfortran-toolchain
	    python))
    (home-page "https://gitlab.com/siesta-project/libraries/xmlf90")
    (synopsis "xmlf90 is a suite of libraries to handle XML in Fortran.")
    (description "xmlf90 is a suite of libraries to handle XML in Fortran.")
    (license license:expat)
  )
)

(define-public libpsml
  (package
    (name "libpsml")
    (version "1.1.12")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://gitlab.com/siesta-project/libraries/libpsml/-/archive/" version  "/" name "-" version ".tar.gz"))
	      (sha256
		(base32
		  "0mjcyhyrkdqvxwxh3a6ih5zbhnc0mjh7mwhv1ybry4fylpah65f6"))))
    (build-system gnu-build-system)
    (arguments
      `(#:configure-flags
	(list (string-append "--prefix=" (assoc-ref %outputs "out"))
	      (string-append "--with-xmlf90=" (assoc-ref %build-inputs "xmlf90")))
	#:tests? #f
	#:phases
        (modify-phases %standard-phases
	  (add-before 'bootstrap 'fix-autoconf
            (lambda _
	      (substitute* "configure.ac"
			   (("1.14")
			   "foreign"))))
          (delete 'check))))
    (inputs
      (list perl 
	    xmlf90
	    libtool
	    autoconf
	    automake
	    gfortran-toolchain
	    python))
    (home-page "https://gitlab.com/siesta-project/libraries/libpsml")
    (synopsis "The libPSML library for handling PSML pseudopotential files.")
    (description "The libPSML library for handling PSML pseudopotential files.")
    (license license:expat)
  )
)

(define-public netcdf-parallel-openmpi-shared
  (package (inherit netcdf)
    (name "netcdf-parallel-openmpi-shared")
    (inputs
     `(("mpi" ,openmpi)
       ,@(alist-replace "hdf5" (list hdf5-parallel-openmpi)
                        (package-inputs netcdf))))
    ;; TODO: Replace pkg-config references in nc-config with absolute references
    (arguments
     (substitute-keyword-arguments (package-arguments netcdf)
       ((#:configure-flags flags)
        `(cons* "CC=mpicc" "CXX=mpicxx"
                "--enable-parallel-tests"
                ;; Shared libraries not supported with parallel IO.
                ,flags))
       ((#:phases phases '%standard-phases)
        `(modify-phases ,phases
           (add-after 'build 'mpi-setup
             ,%openmpi-setup)))))))


(define-public netcdf-fortran-parallel-openmpi
  (package
    (name "netcdf-fortran-parallel-openmpi")
    (version "4.5.3")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-fortran-"
                    version ".tar.gz"))
              (sha256
               (base32
                "0x4acvfhbsx1q79dkkwrwbgfhm0w5ngnp4zj5kk92s1khihmqfhj"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags 
         (list (string-append "--prefix=" (assoc-ref %outputs "out"))
	       (string-append "LIBS=-L" (assoc-ref %build-inputs "netcdf-parallel-openmpi-shared") "/lib -lnetcdf"))
       #:phases
         (modify-phases %standard-phases
           (add-before 'configure 'setenv
             (lambda _
		(setenv "FCFLAGS" "-fallow-argument-mismatch")
		(setenv "FFLAGS" "-fallow-argument-mismatch")
		(setenv "FC" "mpif90")
		(setenv "CC" "mpicc")
		(setenv "LDFLAGS" "mpif90")
		(setenv "LDFLAGS" (string-append "-L" (assoc-ref %build-inputs "netcdf-parallel-openmpi-shared") "/lib"))
		(setenv "CPPFLAGS" (string-append "-I" (assoc-ref %build-inputs "netcdf-parallel-openmpi-shared") "/include"))
	        (setenv "LIBS" (string-append "-L" (assoc-ref %build-inputs "netcdf-parallel-openmpi-shared") "/lib -lnetcdf"))))) 
       #:parallel-tests? #f))
    (inputs
     (list netcdf-parallel-openmpi-shared
	   openmpi
	   gfortran-toolchain))
    (native-inputs
     (list gfortran))
    (synopsis "Fortran interface for the netCDF library")
    (description (package-description netcdf))
    (home-page (package-home-page netcdf))
    (license (package-license netcdf))))

(define-public siesta
  (package
    (name "siesta")
    (version "4.1.5")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://gitlab.com/siesta-project/siesta/-/releases/v" version  "/downloads/" name "-" version ".tar.gz"))
	      (sha256
		(base32
		  "0gqb2k3x0hyp759iy5w7nhjsk29cgd9vnfn79kigafi1lqdg73ai"))))
    (build-system gnu-build-system)
    (arguments
      `(#:tests? #f
	#:phases
        (modify-phases %standard-phases
	  (add-before 'configure 'arch-writing
            (lambda* (#:key inputs outputs #:allow-other-keys)
	      (chdir "Obj")
	      (call-with-output-file "arch.make"
              (lambda (port)
                (format port "		
.SUFFIXES:
.SUFFIXES: .f .F .o .c .a .f90 .F90

SIESTA_ARCH = unknown

CC = mpicc
FPP = $(FC) -E -P -x c
FC = mpifort
FC_SERIAL = gfortran

FFLAGS = -O2 -fPIC -ftree-vectorize -march=native -fallow-argument-mismatch

AR = ar
ARFLAGS_EXTRA =
RANLIB = ranlib
SYS = nag

SP_KIND = 4
DP_KIND = 8
KINDS = $(SP_KIND) $(DP_KIND)

LDFLAGS =
# Dependency rules ---------

FFLAGS_DEBUG = -g -O1 -fallow-argument-mismatch  # your appropriate flags here...

#---------------------------------------------
# These are non-optimized libraries. You should
# use optimized versions for production runs.
#
#COMP_LIBS = libsiestaLAPACK.a libsiestaBLAS.a

FPPFLAGS := $(DEFS_PREFIX)-DFC_HAVE_ABORT $(FPPFLAGS)
FPPFLAGS := $(DEFS_PREFIX)-D__NO_PROC_POINTERS__ $(FPPFLAGS)

LIBS =

# This (as well as the -DMPI definition) is essential for MPI support
MPI_INTERFACE = libmpi_f90.a
MPI_INCLUDE = .
FPPFLAGS +=  -DMPI -DNCDF_PARALLEL -DMPI_TIMING

#math
BLAS_LIBS = -L~a/lib -lblas
LAPACK_LIBS = -L~a/lib -llapack
SCALAPACK_LIBS = -L~a/lib -lscalapack
LIBS += $(SCALAPACK_LIBS) $(LAPACK_LIBS) $(MPI_LIBS) $(COMP_LIBS) $(BLAS_LIBS)

#netcdf
INCFLAGS += -I~a/include
LDFLAGS += -L~a/lib -Wl,-rpath=~a/lib
LDFLAGS += -L~a/lib -Wl,-rpath=~a/lib
LDFLAGS += -L~a/lib -Wl,-rpath=~a/lib
LIBS += -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz
COMP_LIBS += libncdf.a libfdict.a
FPPFLAGS += -DCDF -DNCDF -DNCDF_4

# gridxc
GRIDXC_ROOT = ~a
include $(GRIDXC_ROOT)/share/org.siesta-project/gridxc_dp_mpi.mk
#include $(GRIDXC_ROOT)/gridxc.mk
LDFLAGS += -Wl,-rpath,~a/lib

#psml
XMLF90_ROOT = ~a
include $(XMLF90_ROOT)/share/org.siesta-project/xmlf90.mk
LDFLAGS += -Wl,-rpath,~a/lib
PSML_ROOT = ~a
include $(PSML_ROOT)/share/org.siesta-project/psml.mk
LDFLAGS += -Wl,-rpath,~a/lib


# The atom.f code is very vulnerable. Particularly the Intel compiler
# will make an erroneous compilation of atom.f with high optimization
# levels.
atom.o: atom.F
	$(FC) -c $(FFLAGS_DEBUG) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_fixed_F) $<

.c.o:
	$(CC) -c $(CFLAGS) $(INCFLAGS) $(CPPFLAGS) $<
.F.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_fixed_F)  $< 
.F90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_free_F90) $< 
.f.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_fixed_f)  $< 
.f90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_free_f90)  $< "
	(assoc-ref inputs "openblas")
	(assoc-ref inputs "lapack")
	(assoc-ref inputs "scalapack")
	(assoc-ref inputs "netcdf-fortran-parallel-openmpi")
	(assoc-ref inputs "zlib")
	(assoc-ref inputs "zlib")
	(assoc-ref inputs "hdf5")
	(assoc-ref inputs "hdf5")
	(assoc-ref inputs "netcdf-fortran-parallel-openmpi")
	(assoc-ref inputs "netcdf-fortran-parallel-openmpi")
	(assoc-ref inputs "libgridxc")
	(assoc-ref inputs "libgridxc")
	(assoc-ref inputs "xmlf90")
	(assoc-ref inputs "xmlf90")
	(assoc-ref inputs "libpsml")
	(assoc-ref inputs "libpsml")


	)))))
	  (replace 'configure
            (lambda _
	      (invoke "sh" "../Src/obj_setup.sh")))
	  (replace 'install
             (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                    (bindir (string-append out "/bin"))
                    (libdir (string-append out "/lib"))
                    (incdir (string-append out "/include")))
		  (install-file "./siesta" bindir)
      (install-file "./arch.make" out)
                    ;(for-each (lambda (f) (install-file f bindir))
                             ;(find-files "../bin/" "."))
                    ;(for-each (lambda (f) (install-file f libdir))
                             ;(find-files "../lib/" "."))
                    ;(for-each (lambda (f) (install-file f incdir))
                             ;(find-files "../include/" "."))
		    )))   
          (delete 'check))))
    (inputs
      (list perl 
	    xmlf90
	    libxc
	    hdf5
	    netcdf
	    libgridxc
	    libpsml
	    zlib
	    netcdf-fortran-parallel-openmpi
	    openmpi
	    autoconf
	    lapack
	    openblas
	    scalapack
	    automake
	    gfortran-toolchain
	    libtool
	    python))
    (home-page "https://gitlab.com/siesta-project/siesta")
    (synopsis "A first-principles materials simulation code using DFT")
    (description "SIESTA is a program for efficient electronic structure calculations
and ab initio molecular dynamics simulations of molecules and
solids in the framework of Density-Functional Theory (DFT).
SIESTA's efficiency stems from the use of a basis set of
strictly-localized atomic orbitals. A very important feature of the
code is that its accuracy and cost can be tuned in a wide range, from
quick exploratory calculations to highly accurate simulations matching
the quality of other approaches, such as plane-wave methods.")
    (license license:gpl3)
  )
)

(define-public siesta-max
  (package
    (name "siesta-max")
    (version "1.0.1")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "https://gitlab.com/siesta-project/siesta/-/archive/MaX-" version "/siesta-MaX-" version ".tar.gz"))
	      (sha256
		(base32
		  "0zw1bh7c7vbydnsm118y15babcr3z2mvjif0gciz20gsqyy002vg"))))
    (build-system gnu-build-system)
    (arguments
      `(#:tests? #f
	#:phases
        (modify-phases %standard-phases
	  (add-before 'configure 'arch-writing
            (lambda* (#:key inputs outputs #:allow-other-keys)
	      (chdir "Obj")
	      (call-with-output-file "arch.make"
              (lambda (port)
                (format port "		
.SUFFIXES:
.SUFFIXES: .f .F .o .c .a .f90 .F90

SIESTA_ARCH = unknown

CC = mpicc
FPP = $(FC) -E -P -x c
FC = mpif90
FC_SERIAL = mpif90

FFLAGS = -O2 -fPIC -ftree-vectorize -march=native -fallow-argument-mismatch

AR = ar
ARFLAGS_EXTRA =
RANLIB = ranlib
SYS = nag

SP_KIND = 4
DP_KIND = 8
KINDS = $(SP_KIND) $(DP_KIND)

LDFLAGS =
# Dependency rules ---------

FFLAGS_DEBUG = -g -O1 -fallow-argument-mismatch  # your appropriate flags here...

#---------------------------------------------
# These are non-optimized libraries. You should
# use optimized versions for production runs.
#
#COMP_LIBS = libsiestaLAPACK.a libsiestaBLAS.a

FPPFLAGS := $(DEFS_PREFIX)-DFC_HAVE_ABORT $(FPPFLAGS)
FPPFLAGS := $(DEFS_PREFIX)-D__NO_PROC_POINTERS__ $(FPPFLAGS)

LIBS =

# This (as well as the -DMPI definition) is essential for MPI support
MPI_INTERFACE = libmpi_f90.a
MPI_INCLUDE = .
FPPFLAGS +=  -DMPI -DNCDF_PARALLEL -DMPI_TIMING

#math
BLAS_LIBS = -L~a/lib -lblas
LAPACK_LIBS = -L~a/lib -llapack
SCALAPACK_LIBS = -L~a/lib -lscalapack
LIBS += $(SCALAPACK_LIBS) $(LAPACK_LIBS) $(MPI_LIBS) $(COMP_LIBS) $(BLAS_LIBS)

#netcdf
INCFLAGS += -I~a/include
LDFLAGS += -L~a/lib -Wl,-rpath=~a/lib
LDFLAGS += -L~a/lib -Wl,-rpath=~a/lib
LDFLAGS += -L~a/lib -Wl,-rpath=~a/lib
LIBS += -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz
COMP_LIBS += libncdf.a libfdict.a
FPPFLAGS += -DCDF -DNCDF -DNCDF_4

# gridxc
GRIDXC_ROOT = ~a
include $(GRIDXC_ROOT)/share/org.siesta-project/gridxc_dp_mpi.mk
#include $(GRIDXC_ROOT)/gridxc.mk
LDFLAGS += -Wl,-rpath,~a/lib

#psml
XMLF90_ROOT = ~a
include $(XMLF90_ROOT)/share/org.siesta-project/xmlf90.mk
LDFLAGS += -Wl,-rpath,~a/lib
PSML_ROOT = ~a
include $(PSML_ROOT)/share/org.siesta-project/psml.mk
LDFLAGS += -Wl,-rpath,~a/lib


# The atom.f code is very vulnerable. Particularly the Intel compiler
# will make an erroneous compilation of atom.f with high optimization
# levels.
atom.o: atom.F
	$(FC) -c $(FFLAGS_DEBUG) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_fixed_F) $<

.c.o:
	$(CC) -c $(CFLAGS) $(INCFLAGS) $(CPPFLAGS) $<
.F.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_fixed_F)  $< 
.F90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_free_F90) $< 
.f.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_fixed_f)  $< 
.f90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_free_f90)  $< "
  	(assoc-ref inputs "openblas")
	(assoc-ref inputs "lapack")
	(assoc-ref inputs "scalapack")
	(assoc-ref inputs "netcdf-fortran-parallel-openmpi")
	(assoc-ref inputs "zlib")
	(assoc-ref inputs "zlib")
	(assoc-ref inputs "hdf5")
	(assoc-ref inputs "hdf5")
	(assoc-ref inputs "netcdf-fortran-parallel-openmpi")
	(assoc-ref inputs "netcdf-fortran-parallel-openmpi")
	(assoc-ref inputs "libgridxc")
	(assoc-ref inputs "libgridxc")
	(assoc-ref inputs "xmlf90")
	(assoc-ref inputs "xmlf90")
	(assoc-ref inputs "libpsml")
	(assoc-ref inputs "libpsml")


	)))))
	  (replace 'configure
            (lambda _
	      (invoke "sh" "../Src/obj_setup.sh")
        (substitute* "Makefile" (("automatic_cell.o atom_options.o") "automatic_cell.o atom_options.o cellsubs.o"))))
	  (replace 'install
             (lambda* (#:key outputs inputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                    (bindir (string-append out "/bin"))
                    (libdir (string-append out "/lib"))
                    (incdir (string-append out "/include")))
		  (install-file "./siesta" bindir)
      (install-file "./arch.make" out)
                    ;(for-each (lambda (f) (install-file f bindir))
                             ;(find-files "../bin/" "."))
                    ;(for-each (lambda (f) (install-file f libdir))
                             ;(find-files "../lib/" "."))
                    ;(for-each (lambda (f) (install-file f incdir))
                             ;(find-files "../include/" "."))
		    )))   
          (delete 'check))))
    (inputs
      (list perl 
	    xmlf90
	    libxc
	    hdf5
	    netcdf
	    libgridxc
	    libpsml
	    zlib
      fftw-fortran-openmpi
	    netcdf-fortran-parallel-openmpi
	    openmpi
	    autoconf
	    lapack
	    openblas
	    scalapack
	    automake
	    gfortran-toolchain
	    libtool
	    python))
    (home-page "https://gitlab.com/siesta-project/siesta")
    (synopsis "A first-principles materials simulation code using DFT - MaX version")
    (description "SIESTA(-MaX) is a program for efficient electronic structure calculations
and ab initio molecular dynamics simulations of molecules and
solids in the framework of Density-Functional Theory (DFT).
SIESTA's efficiency stems from the use of a basis set of
strictly-localized atomic orbitals. A very important feature of the
code is that its accuracy and cost can be tuned in a wide range, from
quick exploratory calculations to highly accurate simulations matching
the quality of other approaches, such as plane-wave methods.")
    (license license:gpl3)
  )
)