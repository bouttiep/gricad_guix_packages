(define-module (common clustalw)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages gcc))

(define-public clustalw
  (package
    (name "clustalw")
    (version "2.1")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "http://www.clustal.org/download/current/clustalw-" version ".tar.gz"))
        (sha256
          (base32 "11llyj08liq0bg6vqan8728qjrbam3xhna2wd6g8rzdbhydhalp0"))))
    (build-system gnu-build-system)
    (propagated-inputs
        `(("gcc" ,gcc "lib")))
    (synopsis "Clustal W multiple sequence alignment programs")
    (description "Clustal aligns sequences using a heuristic that progressively builds a multiple sequence 
      alignment from a set of pairwise alignments. This method works by analyzing the sequences as a whole 
      and using the UPGMA/neighbor-joining method to generate a distance matrix. A guide tree is calculated 
      from the scores of the sequences in the matrix, then subsequently used to build the multiple sequence 
      alignment by progressively aligning the sequences in order of similarity.")
    (home-page "http://www.clustal.org/download/current/")
    (license license:gpl3+)))

;; This allows you to run guix shell -f cosma.scm.
;; Remove this line if you just want to define a package.
clustalw