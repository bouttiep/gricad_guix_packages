(define-module (common python-pyscf)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (common gmt)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages python)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages check)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages time)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages geo))

(define-public libcint
  (package
    (name "libcint")
    (version "5.1.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/sunqm/libcint")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1dasic92midk7akgg22g7ka25qfzx7x0whf0nj6fdb4n337v6r5w"))
        )
      )
    (build-system cmake-build-system)
    (arguments
      `(#:phases
        (modify-phases %standard-phases
          (delete 'check))))
    (native-inputs (list python
			 clisp))
    (home-page "https://github.com/sunqm/libcint")
    (synopsis "libcint is an open source library for analytical Gaussian integrals.")
    (description "libcint is an open source library for analytical Gaussian integrals. It provides C/Fortran API to evaluate one-electron / two-electron integrals for Cartesian / real-spheric / spinor Gaussian type functions.")
    (license license:bsd-2)
  )
)

(define-public libxc
  (package
    (name "libxc")
    (version "5.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ElectronicStructureLibrary/libxc")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
        (base32 "1zfhfiwk8cnfbmkagaia4xhsc133icl1pryzync28kzsjxzlwfzc"))
      )
    )
    (build-system cmake-build-system)
    (arguments
      `(#:phases
        (modify-phases %standard-phases
          (delete 'check))))
    (home-page "https://github.com/ElectronicStructureLibrary/libxc")
    (synopsis "Libxc is a library of exchange-correlation functionals for density-functional theory.")
    (description "Libxc is a library of exchange-correlation functionals for density-functional theory. The aim is to provide a portable, well tested and reliable set of exchange and correlation functionals that can be used by a variety of programs.")
    (license license:mpl2.0)
  )
)

(define-public xcfun
  (package
    (name "xcfun")
    (version "cmake-3.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/fishjojo/xcfun/")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
        (base32 "1nspbyygsaq6b3yxgb0dxqc7h8p2sd7ifsc7rxzniakfz5bz5k43"))
      )
    )
    (build-system cmake-build-system)
    (arguments
      `(#:phases
        (modify-phases %standard-phases
          (delete 'check))))
    (home-page "https://github.com/fishjojo/xcfun/")
    (synopsis "Libxc is a library of exchange-correlation functionals for density-functional theory.")
    (description "Libxc is a library of exchange-correlation functionals for density-functional theory. The aim is to provide a portable, well tested and reliable set of exchange and correlation functionals that can be used by a variety of programs.")
    (license license:mpl2.0)
  )
)

(define-public python-pyscf
  (package
    (name "python-pyscf")
    (version "2.1.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/pyscf/pyscf/archive/refs/tags/v" version  ".tar.gz"))
              (sha256
               (base32
                "105jxisa9sdmli6phhhyrsynxy899j8y6r42xrmajyzmffm40hl1"))))
    (build-system python-build-system)
    (arguments
      `(#:phases
	(modify-phases %standard-phases
         (delete 'check)
	 (replace 'build 
            (lambda* (#:key outputs inputs version #:allow-other-keys)
              (let ((out (assoc-ref outputs "out")))
	       (setenv "CMAKE_CONFIGURE_ARGS"
		       (string-append "-DBUILD_LIBCINT=0 -DBUILD_LIBXC=0 -DBUILD_XCFUN=0 -DCMAKE_INSTALL_PREFIX:PATH=" (assoc-ref %outputs "out")))
	       (apply invoke "python" (list "setup.py" "build"))))))))
    (native-inputs (list cmake))
    (inputs (list libxc
                  libcint
                  xcfun
		  openblas))
    (propagated-inputs (list python-h5py python-numpy python-scipy))
    (home-page "http://www.pyscf.org")
    (synopsis "PySCF: Python-based Simulations of Chemistry Framework")
    (description "PySCF: Python-based Simulations of Chemistry Framework")
    (license #f)
  )
)
