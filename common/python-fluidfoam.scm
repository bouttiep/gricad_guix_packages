(define-module (common python-fluidfoam)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system python)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages check))

;; package python-fluidfoam
(define-public python-fluidfoam
  (package
    (name "python-fluidfoam")
    (version "0.2.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/fluiddyn/fluidfoam")
              (commit "2be48aae04839b49ad4235c75a1188bf1d251276")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1l1ivfhwv4y440wr7v3ilcxwibx5b3bzq1cd29spqzjmwmgiwbp0"))))
    (build-system python-build-system)    
    (inputs
     (list python-wrapper
           python-netcdf4
           python-numpy
           python-scipy
           python-matplotlib))
    (arguments (list  
              #:phases #~(modify-phases %standard-phases
                    ;; On remove la phase de configuration
                    (delete 'check)
                   ; On patche le HOME
                   (add-before 'build 'patch-HOME-path
                      (lambda _
                        (setenv "HOME" "/tmp")))
                    ;; opensm is needed for InfiniBand support.
                    (replace 'build
                      (lambda _
                       ;; Test
                       (invoke "python" "setup.py" "develop" "--user")))
                    )))
    (home-page "https://github.com/fluiddyn/fluidfoam")
    (synopsis "The fluidfoam package provides Python classes useful to perform some plot with OpenFoam data.")
    (description "The fluidfoam package provides Python classes useful to perform some plot with OpenFoam data.")
    (license license:gpl3+)))
