(define-module (common openfoam-custom)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix build utils)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages simulation))

;; This is a fork of 'openfoam-org', maintained separately.
(define (make-openfoam version hash)
  (package
    (inherit openfoam-com)
    (name "openfoam-custom")
    (version version)
    (source (origin
              (method url-fetch)
              (uri (string-append "https://develop.openfoam.com"
                                  "/Development/openfoam/-/archive/OpenFOAM-v"
                                  version
                                  "/openfoam-OpenFOAM-v"
                                  version
                                  ".tar.gz"))
              (sha256 (base32 hash))
              (modules '((guix build utils)))
              (snippet `(begin
                          (substitute* "etc/bashrc"
                            ;; set same version as guix package
          (("^export WM_PROJECT_VERSION=.*$")
           (string-append "export WM_PROJECT_VERSION="
              ,version "\n")))
                          ;; patch shell paths
                          (substitute* (list "src/OSspecific/POSIX/POSIX.C"
                                             "wmake/src/Makefile"
                                             "wmake/makefiles/general"
                                             "wmake/makefiles/info")
                            (("/bin/sh")
                             "sh"))))))
    (synopsis "Framework for numerical simulation of fluid flow (from openfoam.com)")
    (home-page "https://www.openfoam.com")))

(define-public openfoam-2312
  (make-openfoam "2312" "1y2bphp1lpisv7mzyqxpgszkjkf4smgr9ddg643fgmwnz9dqn8nd"))

(define-public openfoam-2406
  (make-openfoam "2406" "1260zl91wfa3f46rmsryxz4gx7si8nvn6bzrm2mb1y2z46xb3yq0"))

(define-public openfoam-custom openfoam-2406)
  
;; Returns the openfoam-custom package
; openfoam-custom