(define-module (common xmi-msim) 
             #:use-module (guix licenses)
             #:use-module (guix packages)
             #:use-module (guix download)
             #:use-module (guix utils)
             #:use-module (guix build utils)
             #:use-module (guix build-system gnu)
             #:use-module (gnu packages)
             #:use-module (gnu packages pkg-config)
             #:use-module (gnu packages gcc)
             #:use-module (gnu packages base)
             #:use-module (gnu packages gnome)
             #:use-module (gnu packages glib)
             #:use-module (gnu packages xml)
             #:use-module (gnu packages maths)
             #:use-module (gnu packages mpi)
             #:use-module (gnu packages commencement))

(define-public xraylib
(package
  (name "xraylib")
  (version "4.0.0")
  (source
    (origin
      (method url-fetch)
      (uri (string-append "http://lvserver.ugent.be/xraylib/xraylib-" version ".tar.gz"))
      (sha256
        (base32
          "0kb7m3ljk2h3gkw142pmxsnsbb1glkkfprg9g7y78whbc5lzv3ap"))))
  (build-system gnu-build-system)
  (arguments '(#:tests? #f))
  (inputs
        `(("gfortran" ,gfortran)))
  (home-page "https://github.com/tschoonj/xraylib/wiki")
  (synopsis
    "xraylib: a library for interactions of X-rays with matter")
  (description
    "xraylib provides access to some of the most respected databases of physical data in the field of X-rays. The core of xraylib is a library, written in ANSI C, containing over 40 functions to be used to retrieve data from these databases. This C library can be directly linked with any program written in C, C++ or Objective-C. Furthermore, the xraylib package contains bindings to several popular programming languages: Fortran 2003, Perl, Python, Java, IDL, Lua, Ruby, PHP and .NET. Although not officially supported, xraylib has been 
     reported to be useable from within Matlab, Mathematica and LabView.")
  (license bsd-4)))

(define-public easyrng
(package
  (name "easyrng")
  (version "1.2")
  (source
    (origin
      (method url-fetch)
      (uri (string-append "https://github.com/tschoonj/easyRNG/releases/download/easyRNG-" version "/easyRNG-" version ".tar.gz"))
      (sha256
        (base32
          "0wj0jr5h36k8zn2i87svgzs9v9bh3a6lpcl3azjk74dv5vhrl9an"))))
  (build-system gnu-build-system)
  (arguments '(#:tests? #f))
  (inputs
        `(("gfortran" ,gfortran)))
  (home-page "https://github.com/tschoonj/easyrng")
  (synopsis
    "easyrng: a lightweight and easy-to-use library that wraps C++11's random number generators, making them available from C and Fortran.")
  (description
    "easyRNG is a simple library for C and Fortran providing access to several pseudo-random number generators and pseudo-random number distributions. It doesn't include any implementations of generators or distributions, but is instead a thin wrapper around the random templates of the C++ Standard Library that have been defined in the 2011 revision of the C++ standard. The API is modelled after the GNU Scientific Library (GSL), and its Fortran bindings FGSL. Preliminary tests suggest that easyRNG's performance of its Mersenne Twister random number generators is only slightly worse than its GSL counterparts, but this stronly depends on which implementation of the C++ Standard Library was used. The main reason to use this library over GSL or FGSL is its license. While GSL and FGSL are covered by the GNU General Public License version 3, easyRNG is available under the 3-clause BSD license, allowing for use in closed-source/commercial software packages.")
  (license bsd-4)))

(define-public xmi-msim
(package
  (name "xmi-msim")
  (version "8.1")
  (source
    (origin
      (method url-fetch)
      (uri (string-append "http://lvserver.ugent.be/xmi-msim/xmimsim-" version ".tar.gz"))
      (sha256
        (base32
          "0vdnw5nhxgir81raa5iarv332iqfs1a41bis21jcv6yyz2n13w0m"))))

  (build-system gnu-build-system)
   (arguments
    `(#:tests? #f
      #:phases
        (modify-phases %standard-phases
          (replace 'build
            (lambda* (#:key inputs outputs #:allow-other-keys)
                     (let ((out (assoc-ref outputs "out")))
              (invoke "make")))))))
  (inputs     
	`(("gfortran" ,gfortran)       
          ("libxml2" ,libxml2)
          ("libxslt" ,libxslt)
          ("hdf5" ,hdf5)
          ("glib" ,glib)
          ("glib:bin" ,glib "bin")
          ("xraylib" ,xraylib)
          ("easyrng" ,easyrng)
          ("pkg-config" ,pkg-config)
;;          ("libsoup" ,libsoup)
          ("gsl" ,gsl)))
  (propagated-inputs
         `(("openmpi",openmpi)))

  (home-page "https://github.com/tschoonj/xmimsim/wiki")
  (synopsis
    "XMI-MSIM: Monte Carlo simulation of energy-dispersive X-ray fluorescence spectrometers")
  (description
     "XMI-MSIM is an open source tool designed for predicting the spectral response of energy-dispersive X-ray fluorescence spectrometers using Monte Carlo simulations. It comes with a fully functional graphical user interface in order to make it as user friendly as possible. Considerable effort has been taken to ensure easy installation on all major platforms")
  (license gpl3)))
