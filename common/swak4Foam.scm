(define-module (common swak4Foam)
  #:use-module (guix)
  #:use-module (guix packages)
  #:use-module (guix hg-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build utils)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages python)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages simulation)
  #:use-module (common openfoam-custom))

(define-public swak4Foam
  (package
  (name "swak4Foam")
  (version "1")
    (source
      (origin
        (method hg-fetch)
        (uri (hg-reference
              (url "http://hg.code.sf.net/p/openfoam-extend/swak4Foam")
              (changeset "fde145032783")))
        (sha256 (base32 "1v0mq4xxixgvwhigipr89w99x6b0glvn0cljif0nilx08m5sjczj"))))
    (propagated-inputs
     (list cmake-minimal
      bison
      flex
      lua
      bear
      openfoam-custom
      python2-minimal
      python-sans-pip
      which
      pkg-config
      ghc-echo
      ))
    (build-system cmake-build-system)
    (arguments '(#:phases (modify-phases %standard-phases
                   ;; On remove la phase de configuration et de check
                   (delete 'configure)
                   (delete 'check)
                   ;; On crée le swakConfig
                   (add-after 'patch-generated-file-shebangs 'create_swak_config
                      (lambda _
                          (invoke "ln" "-s" "swakConfiguration.automatic" "swakConfiguration")))
                   ; On remplace le /bin/echo
                   (add-after 'create_swak_config 'fix_interpreters
                      (lambda _
                          (invoke "sed" "-i" (string-append "s@pkg-config@" (which "pkg-config") "@g") "swakConfiguration")
                          (invoke "sed" "-i" (string-append "s@/bin/echo@" (which "echo") "@g") "Libraries/rules/versionOptions")))
                   ; On patche le HOME
                   (add-after 'fix_interpreters 'patch-HOME-path
                      (lambda _
                        (setenv "HOME" (getenv "TMP"))
                        (setenv "FOAM_MODULE_PREFIX" (getenv "out"))))
                   ; On patch wmake dans librairies
                   (add-before 'build 'patch-wmake-libraries
                      (lambda _
                        (invoke "sed" "-i" "109,114 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of swakLagrangianParser
                        (invoke "sed" "-i" "118 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of swakFunctionObjects
                        (invoke "sed" "-i" "120 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of simpleFunctionObjects
                        (invoke "sed" "-i" "131 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of simpleSwakFunctionObjects   
                        (invoke "sed" "-i" "167 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of swakStateMachine   
                        (invoke "sed" "-i" "171 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of swakDecomposition   
                        (invoke "sed" "-i" "173 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of languageIntegration folder
                        (invoke "sed" "-i" "199 s/^/#/" "Libraries/Allwmake") ;; to disable compilation of swakCloudFunctionObjects
                      ))
                   ;; On remplace la phase de build
                   (replace 'build
                     (lambda _
                       ;; compile OpenFOAM libraries and applications
                       (invoke "bash" "-c"
                               (format #f
                                       "source ~a && 
                                        export FOAM_USER_LIBBIN=$FOAM_MODULE_PREFIX/share/$WM_PROJECT/platforms/$WM_OPTIONS/lib && 
                                        export FOAM_USER_APPBIN=$FOAM_MODULE_PREFIX/share/$WM_PROJECT/platforms/$WM_OPTIONS/bin && 
                                        ./Allwmake > log.make 2>&1" (string-append (assoc-ref %build-inputs "openfoam-custom") "/share/OpenFOAM/etc/bashrc")))))
                   ;; On remplace la phase d'install
                   (replace 'install
                     (lambda* (#:key outputs inputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                        (install-dir (string-append out "/share/swak4Foam")))
                        ; (mkdir-p install-dir) ;create install directory
                        ;; move contents of build directory to install directory
                        (copy-recursively "../OpenFOAM" out))))
                   )))
    (home-page "https://openfoamwiki.net/index.php/Contrib/swak4Foam")
    (synopsis "A library that combines the functionality of groovyBC and funkySetFields")
    (description "A library that combines the functionality of groovyBC and funkySetFields: 
      it offers the user the possibility to specify expressions involving the fields and evaluates them. 
      This library offers a number of utilities (for instance funkySetFields to set fields using expression), 
      boundary conditions (groovyBC to specify arbitrary boundary conditions based on expressions) and function 
      objects that allow doing many things that would otherwise require programming. ")
    (license license:gpl3+)))

;; This allows you to run guix shell -f 2decomp_fft.scm.
;; Remove this line if you just want to define a package.
swak4Foam
