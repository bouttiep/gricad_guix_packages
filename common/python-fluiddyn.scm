(define-module (common python-fluiddyn)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system meson)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (common python-packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages check)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages mpi))

(define-public python-shtns
  (package
    (name "python-shtns")
    (version "3.6.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "shtns" version))
       (sha256
        (base32 "150hswhs7nv5ipkm786kif51s9rnqnpf5mpwhy0z5sc2mcxd640w"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-numpy
              fftw))
    (native-inputs 
        (list python-setuptools
              python-setuptools-scm
              python-wheel))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Add a phase to fix /bin/sh interpreter
              (add-after 'unpack 'fix_binsh
                  (lambda* (#:key inputs #:allow-other-keys)
                      (for-each (lambda (f)
                          (invoke "sed" "-i" (string-append "s@/bin/sh@" (which "sh") "@g") f))
                              (find-files "."))))
          )))
    (home-page "https://bitbucket.org/nschaeff/shtns")
    (synopsis "High performance Spherical Harmonic Transform")
    (description "This is the python module to use SHTns, a high performance spherical 
      harmonic transform and rotation library. It requires FFTW installed in your system.")
    (license license:cecill-b)))

;; This is a copy-paste of the package definition on the main guix channel
;; The version is the only thing that is different 
(define-public python-scikit-build-14
  (package
    (name "python-scikit-build-14")
    (version "0.14.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "scikit-build" version))
       (sha256
        (base32 "1wx1m9vnxnnz59lyaisgyxldp313kciyd4af8lf112vb8vbjy9yk"))))
    (build-system python-build-system)
    (native-inputs
        (list cmake-minimal
              gfortran
              git-minimal
              ninja
              python-coverage
              python-cython
              python-mock
              python-packaging
              python-path
              python-pypa-build
              python-pytest
              python-pytest-cov
              python-pytest-mock
              python-pytest-virtualenv
              python-pytest-xdist
              python-requests
              python-setuptools-scm
              python-wheel
              python-distro))
    (arguments
        (list #:phases #~(modify-phases %standard-phases
            (add-after 'unpack 'patch-cmake-executable
                (lambda* (#:key inputs #:allow-other-keys)
                    (substitute* "skbuild/constants.py"
                    (("^(CMAKE_DEFAULT_EXECUTABLE = ).*" _ head)
                    (format #f "~a ~s~%" head
                        (search-input-file inputs "bin/cmake"))))))
        ;; XXX: PEP 517 manual build copied from python-isort.
        (replace 'build
            (lambda _
                (setenv "SOURCE_DATE_EPOCH" "315532800")
                (invoke "python" "-m" "build" "--wheel" "--no-isolation" ".")))
        (replace 'check
            (lambda* (#:key tests? #:allow-other-keys)
                (when tests?
                    ;; These tests attempt to pull dependencies from the Internet.
                    (delete-file "tests/test_distribution.py")
                    (delete-file "tests/test_pep518.py")
                    (invoke "pytest" "-vv"
                        "-n" (number->string (parallel-job-count))
                        "-k" (string-append
                            ;; These tests attempt to write to read-only
                            ;; Python install directory.
                            "not test_install_command "
                            "and not test_test_command "
                            "and not test_hello_develop "
                            ;; These sdist-related tests fail for unknown
                            ;; reasons (see:
                            ;; https://github.com/scikit-build/scikit-build/issues/689).
                            "and not test_hello_sdist_with_base "
                            "and not test_manifest_in_sdist "
                            "and not test_hello_sdist "
                            "and not test_sdist_with_symlinks "
                            ;; The reason for the failure of this one is
                            ;; also unknown.
                            "and not test_generator_cleanup")))))
        (replace 'install
            (lambda _
                (let ((whl (car (find-files "dist" "\\.whl$"))))
                (invoke "pip" "--no-cache-dir" "--no-input"
                        "install" "--no-deps" "--prefix" #$output whl))))
        ;; Remove check and sanity-check phases
        (delete 'check)
        (delete 'sanity-check))))
    (home-page "https://github.com/scikit-build/scikit-build")
    (synopsis "Build system generator for Python C/C/Fortran/Cython extensions")
    (description "Scikit-build is an improved build system generator for CPython C/C/Fortran/Cython extensions.  It has support for additional
compilers, build systems, cross compilation, and locating dependencies and determining their build requirements.  The scikit-build package is
fundamentally just glue between the @code{setuptools} Python module and CMake.")
    (license license:expat)))

;; downgrade python-opencv version to the closest of the release of python 3.10.7 ...
;; Unfortunately, the package is not ready yet
;; Got multiple errors depending on the version used :
;;      - recursive import of cv2
;;      - issue with version of scikit (causing trouble for building cv2)
;;      - issue with version (too recent) of cv2
(define-public python-opencv
  (package
    (name "python-opencv")
    ; (version "4.7.0.72")
    (version "4.5.5.64")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "opencv-python" version))
       (sha256
        (base32 "1xxks017dqq409vljgxcvhaipknq8l1s2jyh7ivkn31kd92f0pgn"))))
        ; (base32 "01ik8my527rr6ndgvmlr5g82gj6gf6qf9hgkh52jhcqzf557j91l")))) ;; for 4.7.0.72
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-numpy
              python-scikit-build-14
              pkg-config
              gtk+
              gstreamer
              zlib
              cmake))
    (native-inputs 
        (list python-setuptools
              python-setuptools-scm
              python-wheel))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Add a phase to configure correctly the build
              (add-before 'build 'fix-output
                  (lambda _
                      (setenv "ZLIB_LIBRARY" (assoc-ref %build-inputs "zlib"))
                      (setenv "ZLIB_INCLUDE_DIR" (string-append (assoc-ref %build-inputs "zlib") "/include"))
                      (setenv "CMAKE_INSTALL_PREFIX" (getenv "out"))
                      (setenv "__INSTALL_PATH_PYTHON3" (getenv "out"))
                      (setenv "CMAKE_ARGS" (string-append 
                              "-DCMAKE_BUILD_TYPE=RELEASE"
                              " -DCMAKE_INSTALL_PREFIX=" (getenv "out")
                              " -DCMAKE_INSTALL_PREFIX:PATH=" (getenv "out")
                              " -DOPENCV_GENERATE_PKGCONFIG=ON"))
                          ; (setenv "PKG_CONFIG_EXECUTABLE" (assoc-ref %build-inputs "pkg-config"))
                          ))
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check))))
    (home-page "https://github.com/opencv/opencv-python")
    (synopsis "Wrapper package for OpenCV python bindings.")
    (description "Wrapper package for @code{OpenCV} python bindings.")
    (license license:asl2.0)))

(define-public python-fluiddyn
  (package
    (name "python-fluiddyn")
    (version "0.6.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluiddyn" version))
       (sha256
        (base32 "0hhk78fbn5k06412p0bnwbqxs22grs0pxlcdlsrm4981a3xb0vjp"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-distro
              python-numpy
              python-matplotlib
              python-h5py
              python-psutil
              python-h5netcdf
              python-mpi4py
              python-ipython
              ; python-opencv
              python-pulp
              python-pyfftw
              python-scikit-image
              python-shtns
              python-simpleeval
              python-pdm-backend))
    (arguments 
        '(#:phases (modify-phases %standard-phases
            ;; Remove check and sanity-check phases
            (delete 'check)
            (delete 'sanity-check))))
    (home-page "https://foss.heptapod.net/fluiddyn/fluiddyn")
    (synopsis "Framework for studying fluid dynamics.")
    (description "FluidDyn project is an ecosystem of packages for research and teaching in 
      fluid dynamics. The Python package fluiddyn contains: basic utilities to manage, i.e. 
      File I/O for some esoteric formats, publication quality figures, job submission on 
      clusters, MPI ; powerful classes to handle, i.e. parameters, arrays, series of files ;
      simplified interfaces to calculate, i.e. FFT, spherical harmonics ; and much more. 
      It is used as a library in the other specialized packages of the FluidDyn project 
      (in particular in fluidfft, fluidsim, fluidlab and fluidimage).")
    (license license:cecill-b)))
