(define-module (common sedfoam)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix build utils)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages simulation)
  #:use-module (gnu packages python)
  #:use-module (common openfoam-custom)
  #:use-module (common swak4Foam)
  #:use-module (guix packages))

(define-public sedfoam
  ;; This is a fork of 'openfoam-org', maintained separately.
  (package
    (name "sedfoam")
    (version "2312")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/SedFoam/sedfoam")
              (commit "6c1629b1c3d586ebe43565f589b30a24c1d53fa3")
              (recursive? #t)))
        (sha256 (base32 "1dmrmxmbipfcy64b5h2ydq8vmxy927lbklrfjxd6ljla87c0r99j"))))
	;; Those two following lines are mandatory for the main branch sedfoam_v2312
        ;;(file-name (git-file-name name version))
        ;;(sha256 (base32 "0sky2dnni75xpcr8fr6dkkxz0q7q7c8rmkz530i8ghl7bwiyj5v1"))))
    (build-system gnu-build-system)
    (propagated-inputs
     (list openfoam-custom
      python-sans-pip
      swak4Foam
      gcc
      ))
    (arguments '(#:phases (modify-phases %standard-phases
                   ;; On remove la phase de configuration et de check
                   (delete 'configure)
                   (delete 'check)
                   ; On patche le HOME
                   (add-before 'build 'patch-HOME-path
                      (lambda _
                        (setenv "HOME" (getenv "out"))
                        (setenv "FOAM_MODULE_PREFIX" (getenv "out"))))
                   ;; On remplace la phase de build
                   (replace 'build
                     (lambda _
                       ;; compile OpenFOAM libraries and applications
                       (invoke "bash" "-c"
                                   (format #f
                                           "source ~a &&
                                           export FOAM_MODULE_PREFIX=$FOAM_MODULE_PREFIX/share/$WM_PROJECT/platforms/$WM_OPTIONS &&
                                           export FOAM_USER_LIBBIN=$FOAM_MODULE_PREFIX/lib &&
                                           export FOAM_USER_APPBIN=$FOAM_MODULE_PREFIX/bin && 
                                           ./Allwmake > log.make 2>&1" (string-append (assoc-ref %build-inputs "openfoam-custom") "/share/OpenFOAM/etc/bashrc")))))
                   ;; On remplace la phase d'install
                   (replace 'install
                     (lambda* (#:key outputs inputs #:allow-other-keys)
                        (let* ((out (assoc-ref outputs "out"))
                               (srcdir (string-append out "/source")))
                        ;; move contents of git clone directory to install directory
                        (copy-recursively "." srcdir)
                        )))
                   )))
    (synopsis "SedFoam, for sediment transport applications")
    (description "A three-dimensional two-phase flow solver, SedFoam, has been developed for sediment transport applications. 
      The solver is extended from twoPhaseEulerFoam available in the 2.1.0 release of the open-source CFD (computational fluid dynamics) 
      toolbox OpenFOAM. In this approach the sediment phase is modeled as a continuum, and constitutive laws have to be prescribed 
      for the sediment stresses. In the proposed solver, two different intergranular stress models are implemented: the kinetic theory 
      of granular flows and the dense granular flow rheology μ(I). For the fluid stress, laminar or turbulent flow regimes can be 
      simulated and three different turbulence models are available for sediment transport: a simple mixing length model (one-dimensional 
      configuration only), a k−ε, a classical k−ω and a k−ω from Wilcox (2006).")
    (home-page "https://sedfoam.github.io/sedfoam/")
    (license license:gpl3+)))

;; This allows you to run guix shell -f 2decomp_fft.scm.
;; Remove this line if you just want to define a package.
sedfoam
