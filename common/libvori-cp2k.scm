;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2024 - Dylan Bissuel <dylan.bissuel@univ-lyon1.fr>

(define-module (common libvori-cp2k)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix build-system cmake))

(define-public libvori-cp2k
  (package
    (name "libvori-cp2k")
    (version "220621")
    (source
      (origin
        (method url-fetch)
        (uri "https://brehm-research.de/files/libvori-220621.tar.gz")
        (sha256 (base32 "052ck0bj7iwjndp7wc7nd237az8kh8az3ry0y6ndsjw1ck2riyhw"))))
    (build-system cmake-build-system)
    (arguments
        (list
            #:tests? #t
            #:build-type "Release"
            #:phases #~(modify-phases %standard-phases
                (replace `check
                    (lambda* (#:key outputs #:allow-other-keys)
                        (invoke "./test01")))
                (replace `install
                    (lambda* (#:key outputs #:allow-other-keys)
                        (let* ((out (assoc-ref outputs "out"))
                               (bindir (string-append out "/bin"))
                               (libdir (string-append out "/lib")))
                        (install-file "./test01" bindir) ; Not sure it's needed but it's the only executable so keeping it for now
                        (install-file "./libvori.a" libdir))))
          )))
    (home-page "https://brehm-research.de/libvori.php")
    (synopsis "Library for Voronoi Integration")
    (description "In principle, libvori enables other programs to include these approaches. The present version of libvori is a very early development version, which is hard-coded to work with the CP2k program package, in which are implemented  methods for Voronoi integration as well as compressed bqb file format . There is no well-defined interface or documentation yet. If you want to use libvori in your code, please have some more patience.")
    (license license:lgpl3)))

;; This allows you to run guix shell -f guix-packager.scm.
;; Remove this line if you just want to define a package.
libvori-cp2k

