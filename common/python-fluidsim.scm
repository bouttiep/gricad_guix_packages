(define-module (common python-fluidsim)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (common python-packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages check)
  #:use-module (common python-fluidfft)
  #:use-module (common python-fluiddyn)
  #:use-module (common python-fluidsim-utils))

(define-public python-fluidsim-core
  (package
    (name "python-fluidsim-core")
    (version "0.8.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidsim_core" version))
       (sha256
        (base32 "0ihn3cyygv6w0y939syw4lms6pg7fqf8wjwaff4l0mqsvgam3yrx"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-fluiddyn 
              python-importlib-metadata))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check))))
    (home-page "https://fluidsim.readthedocs.io/en/latest/generated/fluidsim_core.html")
    (synopsis "Pure-Python core library for FluidSim framework")
    (description "Pure-Python core library for FluidSim framework. This package 
      provides generic base classes and utilities to build new solvers.")
    (license license:cecill-b)))

;; package python-fluidsim
(define-public python-fluidsim
  (package
    (name "python-fluidsim")
    (version "0.8.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "fluidsim" version))
       (sha256
        (base32 "0yw51gwil4gg2ghm68bd6z8xf970nbfmqw14vzsmd622ihw0zis4"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-fluidfft
              python-fluidsim-core
              python-h5netcdf
              python-h5py
              python-ipython
              python-matplotlib
              python-mpi4py
              python-pyfftw
              python-pymech
              python-rich
              python-scipy
              python-transonic
              python-xarray))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Add a phase to patch $HOME environment variable
              (add-before 'build 'patch-HOME-path
                  (lambda _
                      (setenv "HOME" (getenv "out"))))
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check))))
    (home-page "https://fluidsim.readthedocs.io/en/latest/")
    (synopsis "Framework for studying fluid dynamics with simulations.")
    (description "Fluidsim is an extensible framework for studying fluid dynamics with numerical simulations 
      using Python. Fluidsim is an object-oriented library to develop Fluidsim 'solvers' (i.e. Python packages 
      solving equations) by writing mainly Python code. The result is very efficient even compared to a pure 
      Fortran or C++ code since the time-consuming tasks are performed by optimized compiled functions.")
    (license license:cecill-b)))
