(define-module (common python-elevation)
#:use-module ((guix licenses) #:prefix license:)
#:use-module (gnu packages)
#:use-module (guix packages)
#:use-module (guix utils)
#:use-module (guix download)
#:use-module (guix build-system python)
#:use-module (gnu packages python-xyz)
#:use-module (gnu packages python)
#:use-module (gnu packages python-build))

(define-public python-elevation
  (package
    (name "python-elevation")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "elevation" version))
        (sha256
          (base32 "0xs8izdklqs9ijz0kb85yhyw92mvvclli357ixylz5p7c9jl89xy"))))
    (build-system python-build-system)
    (propagated-inputs (list python-appdirs python-click python-fasteners))
    (home-page "http://elevation.bopen.eu")
    (synopsis
      "Python script to download global terrain digital elevation models, SRTM 30m DEM and SRTM 90m DEM.")
    (description
      "Python script to download global terrain digital elevation models, SRTM 30m DEM
and SRTM 90m DEM.")
    (license #f)))
