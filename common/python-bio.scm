(define-module (common python-bio)
   #:use-module  ((guix licenses) #:prefix license:)
   #:use-module  (gnu packages)
   #:use-module  (guix packages)
   #:use-module  (guix download)
   #:use-module  (guix build-system python)
   #:use-module  (guix build-system pyproject)
   #:use-module  (gnu packages python)
   #:use-module  (gnu packages python-science)
   #:use-module  (gnu packages python-xyz)
   #:use-module  (gnu packages python-web)
   #:use-module  (gnu packages python-build)
   #:use-module  (gnu packages bioinformatics))


(define-public python-gprofiler-official
  (package
    (name "python-gprofiler-official")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "gprofiler-official" version))
       (sha256
        (base32 "0pa4ybg5v12vyxa4366kgkjznyn0r4ayhhp3ayfbbp7v21zv85ah"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-requests))
    (native-inputs (list python-setuptools python-wheel))
    (home-page "https://biit.cs.ut.ee/gprofiler/")
    (synopsis
     "Functional enrichment analysis and more via the g:Profiler toolkit")
    (description
     "Functional enrichment analysis and more via the g:Profiler toolkit.")
    (license license:bsd-1)))

(define-public python-bio
  (package
    (name "python-bio")
    (version "1.7.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "bio" version))
       (sha256
        (base32 "0917v8hcskmxq8jxyclnn03szm8rslgwjq1prag767hbbf854cnz"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-biopython
                             python-gprofiler-official
                             python-mygene
                             python-pandas
                             python-pooch
                             python-requests
                             python-tqdm))
    (native-inputs (list python-hatchling))
    (arguments 
        '(#:phases (modify-phases %standard-phases
          ;; Remove check phase "1. &test-system-not-found" error
          (delete 'check))))
    (home-page "https://github.com/ialbert/bio/")
    (synopsis "Making bioinformatics fun again ")
    (description "Like LEGO pieces that match one another bio aims to provide you with commands that naturally fit 
      together and let you express your intent with short, explicit and simple commands. It is a project in an 
      exploratory phase, we'd welcome input and suggestions on what it should grow up into.")
    (license license:expat)))