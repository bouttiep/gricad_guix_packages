(define-module (common cdhit) 
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages pkg-config))

(define-public cd-hit
  (package
    (name "cd-hit")
    (version "4.8.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/weizhongli/cdhit/archive/refs/tags/V" version ".tar.gz"))
              (sha256
               (base32
                "0w0bnb6mfvn0xcvsilmv6qg3yhd6wy9h1vaysgy35d7bgbfkrg7q"))))
    (build-system gnu-build-system)
    (inputs
      `(("zlib" ,zlib)))   
    (propagated-inputs
    `(("perl" ,perl)))
    (arguments
     `(#:make-flags
        (list (string-append "PREFIX="
                             (assoc-ref %outputs "out") "/bin"))
       #:phases
       (modify-phases %standard-phases
         (delete 'check)
         (replace 'configure
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               (mkdir-p (string-append out "/bin"))))))))
    (synopsis "CD-HIT is a very widely used program for clustering and comparing protein or nucleotide sequences.")
    (description "CD-HIT is very fast and can handle extremely large databases. CD-HIT helps to significantly reduce the computational and manual efforts in many sequence analysis tasks and aids in understanding the data structure and correct the bias within a dataset.")
    (home-page "http://weizhong-lab.ucsd.edu/cd-hit/")
    (license license:gpl3+)))