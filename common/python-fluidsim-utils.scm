(define-module (common python-fluidsim-utils)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (common python-packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages web)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages sphinx))

(define-public python-sphinx-theme-builder
  (package
    (name "python-sphinx-theme-builder")
    (version "0.2.0b2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "sphinx-theme-builder" version))
       (sha256
        (base32 "127jls164jnbd9hczzyi1040zhfw7h29lii1wx7l3grmpg19ikg9"))))
    (build-system pyproject-build-system)
    (inputs 
        (list python-flit-core))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check))))
    (home-page "https://sphinx-theme-builder.readthedocs.io/en/latest/")
    (synopsis "A tool for authoring Sphinx themes with a simple (opinionated) workflow.")
    (description "This package provides a tool for authoring Sphinx themes with a simple
      (opinionated) workflow.")
    ;; should be MIT licence
    (license license:ogl-psi1.0)))

(define-public python-asv
  (package
    (name "python-asv")
    (version "0.6.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "asv" version))
       (sha256
        (base32 "0p6k8v9wwbxzpzjgyla095mkw4984wzvjfmky7cij8126ky13v3g"))))
    (build-system pyproject-build-system)
    (native-inputs
        (list python-setuptools
              python-setuptools-scm
              python-wheel))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check))))
    (home-page "https://asv.readthedocs.io/en/stable/")
    (synopsis "Airspeed Velocity: A simple Python history benchmarking tool")
    (description "Airspeed Velocity (asv) is a tool for benchmarking Python packages over their lifetime.
      It is primarily designed to benchmark a single project over its lifetime using a given suite of benchmarks. 
      The results are displayed in an interactive web frontend that requires only a basic static webserver to host.")
    (license license:bsd-3)))


(define-public python-pymech
  (package
    (name "python-pymech")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pymech" version))
       (sha256
        (base32 "0qlh7iw26dnp5p3wrgybw3a8w64cfras4c403gg07dxf298axd3f"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-attrs 
              python-importlib-metadata
              python-numpy
              python-xarray))
    (native-inputs 
        (list python-asv
              python-coverage
              python-dask
              python-matplotlib
              python-mypy
              ; python-nox
              python-numpy
              python-pyperf
              python-pytest
              python-pytest-cov
              python-pytest-xdist
              python-rich
              python-sphinx-theme-builder
              python-typing-extensions
              python-virtualenv
              python-setuptools
              python-setuptools-scm
              python-wheel))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check))))
    (home-page "https://github.com/eX-Mech/pymech")
    (synopsis "A Python suite of routines for Nek5000 and Simson.")
    (description "This package provides a Python suite of routines for Nek5000 and Simson.")
    (license license:gpl3+)))


(define-public python-pytest-allclose
  (package
    (name "python-pytest-allclose")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pytest-allclose" version))
       (sha256
        (base32 "0hdhqsffc2rqc6cfxkrvk45wbcsd8n25q42a1m0828k5z8hwbw5j"))))
    (build-system pyproject-build-system)
    (propagated-inputs 
        (list python-numpy 
              python-pytest))
    (native-inputs 
        (list python-codespell  
              python-coverage 
              python-flake8
              python-pylint
              python-setuptools
              python-setuptools-scm
              python-wheel))
    (arguments 
        '(#:phases (modify-phases %standard-phases
              ;; Remove check and sanity-check phases
              (delete 'check)
              (delete 'sanity-check))))
    (home-page "https://www.nengo.ai/pytest-allclose")
    (synopsis "Pytest fixture extending Numpy's allclose function")
    (description "Pytest-allclose provides the ~.allclose Pytest fixture, extending numpy.allclose 
      with test-specific features. A core feature of the ~.allclose fixture is that the tolerances 
      for tests can be configured externally. This allows different repositories to share the same 
      tests, but use different tolerances. See the “Configuration” section below for details.")
    (license license:expat)))
