(define-module (common getdp)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages gcc))

(define-public getdp
  (package
    (name "getdp")
    (version "3_5_0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://gitlab.onelab.info/getdp/getdp")
              (commit (string-append name "_" version))))
        (file-name (git-file-name name version))
        (sha256 (base32 "03lgbq7nvzmkhn395rmczvypi8wz9pppdg29jpj4jxfp4xdsqhcb"))))
    (build-system cmake-build-system)
    (propagated-inputs
     (list gsl
           petsc
           gfortran
           openblas
           python-sans-pip
           ))
    (synopsis "General environment for the treatment of Discrete Problems (GetDP)")
    (description "GetDP is a free finite element solver using mixed elements to discretize de Rham-type complexes in one, 
      two and three dimensions. The main feature of GetDP is the closeness between the input data defining discrete problems 
      (written by the user in ASCII data files) and the symbolic mathematical expressions of these problems. ")
    (home-page "http://getdp.info/")
    (license license:gpl2)))