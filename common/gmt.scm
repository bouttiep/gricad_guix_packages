(define-module (common gmt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages image)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages video)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages multiprecision))

(define-public dcw-gmt
  (package
  (name "dcw-gmt")
  (version "2.0.0")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/GenericMappingTools/dcw-gmt/releases/download/" version "/dcw-gmt-" version ".tar.gz"))
            (sha256
             (base32
              "0ync8qr8qad1qim4k3mnnq7cks16pfnkzh3k0znmz03shff207fp"))))
  (build-system gnu-build-system)
  (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (delete 'build)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               (copy-recursively "." (string-append out "/share/dcw-gmt"))))))))
  (synopsis "Vector basemap of the world, for use with GMT")
  (description "DCW-GMT is an enhancement to the original 1:1,000,000 scale vector basemap
      of the world, available from the Princeton University Digital Map and
      Geospatial Information Center. It contains more state boundaries (the
      largest 8 countries are now represented) than the original data
      source. Information about DCW can be found on Wikipedia
      (https://en.wikipedia.org/wiki/Digital_Chart_of_the_World). This data is
      for use by GMT, the Generic Mapping Tools.")
  (home-page "https://www.soest.hawaii.edu/pwessel/dcw/")
  (license license:gpl3+)))

(define-public gshhg-gmt
  (package
  (name "gshhg-gmt")
  (version "2.3.7")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/GenericMappingTools/gshhg-gmt/releases/download/" version "/gshhg-gmt-" version ".tar.gz"))
            (sha256
             (base32
              "0zzy5jlw4hmh13gpbwc1rq05llwpaxi2x17g7c48qwd0zibakccv"))))
  (build-system gnu-build-system)
  (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (delete 'build)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               (copy-recursively "." (string-append out "/share/dcw-gmt"))))))))
  (synopsis "High-resolution shoreline data set, for use with GMT")
  (description "GSHHG is a high-resolution shoreline data set amalgamated from two
      databases: Global Self-consistent Hierarchical High-resolution Shorelines
      (GSHHS) and CIA World Data Bank II (WDBII). GSHHG contains vector
      descriptions at five different resolutions of land outlines, lakes,
      rivers, and political boundaries. This data is for use by GMT, the Generic
      Mapping Tools.")
  (home-page "https://www.soest.hawaii.edu/pwessel/gshhg/")
  (license license:gpl3+)))

(define-public gmt
  (package
  (name "gmt")
  (version "6.3.0")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/GenericMappingTools/gmt/releases/download/" version "/gmt-" version "-src.tar.gz"))
            (sha256
             (base32
              "1l88ibn04f72cwr6m92m5c6r9irrisppmaiqkfjqf6zin7c77l1c"))))
  (build-system cmake-build-system)
  (arguments
   `(#:configure-flags
      (list
        (string-append "-DGSHHG_ROOT="
          (assoc-ref %build-inputs "gshhg-gmt") "/share/gshhg-gmt")
        (string-append "-DDCW_ROOT="
          (assoc-ref %build-inputs "dcw-gmt") "/share/dcw-gmt"))
      #:tests? #f))
  (inputs
    `(("openblas" ,openblas)
      ("curl" ,curl)
      ("fftw" ,fftw)
      ("pcre" ,pcre)
      ("gdal" ,gdal)
      ("netcdf" ,netcdf)
      ("dcw-gmt" ,dcw-gmt)
      ("gshhg-gmt" ,gshhg-gmt)
      ("ffmpeg" ,ffmpeg)
      ("lapack" ,lapack)))
  (synopsis "Tools for manipulating geographic and cartesian data sets")
  (description "GMT is an open-source collection of command-line tools for manipulating
      geographic and Cartesian data sets (including filtering, trend fitting,
      gridding, projecting, etc.) and producing high-quality illustrations
      ranging from simple x–y plots via contour maps to artificially illuminated
      surfaces and 3D perspective views. It supports many map projections and
      transformations and includes supporting data such as coastlines, rivers,
      and political boundaries and optionally country polygons.")
  (home-page "https://www.generic-mapping-tools.org")
  (license license:gpl3+)))