(define-module (common python-eccodes)
        #:use-module (guix licenses)
        #:use-module (guix packages)
        #:use-module (guix download)
      	#:use-module (guix utils)
        #:use-module (guix build utils)
        #:use-module (guix build-system python)
        #:use-module (gnu packages)
        #:use-module (gnu packages pkg-config)
        #:use-module (gnu packages python)
        #:use-module (gnu packages python-xyz)
        #:use-module (gnu packages libffi)
        #:use-module (gnu packages commencement))


(define-public python-findlibs
  (package
    (name "python-findlibs")
    (version "0.0.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "findlibs" version))
              (sha256
               (base32
                "139c0gj4lskbjxyb23lbpnzda8l5nrmp7m9cmf1pgagrjs206zkc"))))
    (build-system python-build-system)
    (home-page "https://github.com/ecmwf/findlibs")
    (synopsis "A packages to search for shared libraries on various platforms")
    (description
     "This package provides a packages to search for shared libraries on various
platforms")
    (license #f)))


(define-public python-eccodes
  (package
    (name "python-eccodes")
    (version "1.4.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "eccodes" version))
              (sha256
               (base32
                "0kv6xgsiy6pbik1aq5g2yhlz0pb0lsk9dhvb917r0idls6hq1yk3"))))
    (build-system python-build-system)
    (arguments 
      `(#:phases
        (modify-phases %standard-phases
         ;; delete tests which fail due to guix store structure, ugly but works
	           (delete 'fix-and-disable-failing-tests)
	           (delete 'sanity-check)
		   (delete 'check)
	)
       )
    )
    (propagated-inputs (list python-attrs python-cffi python-findlibs
                             python-numpy))
    (home-page "https://github.com/ecmwf/eccodes-python")
    (synopsis "Python interface to the ecCodes GRIB and BUFR decoder/encoder")
    (description
     "Python interface to the ecCodes GRIB and BUFR decoder/encoder")
    (license #f)))
