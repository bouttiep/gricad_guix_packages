(define-module (common benchmark)
      #:use-module ((guix licenses) #:prefix license:)
      #:use-module (guix packages)
      #:use-module (guix download)
      #:use-module (guix utils)
      #:use-module (guix build utils)
      #:use-module (guix build-system cmake)
      #:use-module (guix build-system gnu)
      #:use-module (gnu packages)
      #:use-module (gnu packages algebra)
      #:use-module (gnu packages image-processing)
      #:use-module (gnu packages image)
      #:use-module (gnu packages graphics)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages mpi)
      #:use-module (gnu packages python)
      #:use-module (gnu packages python-xyz)
      #:use-module (gnu packages compression)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages pkg-config)
      #:use-module (gnu packages video)
      #:use-module (gnu packages llvm)
      #:use-module (gnu packages multiprecision))

(define-public osu-benchmarks
 (package
    (name "osu-benchmarks")
    (version "5.8")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-" version ".tgz"))
              (sha256
              (base32
                "19a4wg0msipibkxsi8i0c34d07512yfaj2k37dxg5541ysdw690f"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'setenv
           (lambda _
             (setenv "CC" (which "mpicc"))
             (setenv "CXX" (which "mpic++"))
             #t)))))
    (propagated-inputs
      `(("openmpi" ,openmpi)))
  (synopsis "OSU micro benchmarks")
  (description "OSU micro benchmark for mpi")
  (home-page "https://mvapich.cse.ohio-state.edu/benchmarks/")
  (license license:gpl2+)))