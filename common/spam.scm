(define-module  (common spam)
  #:use-module (guix packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build utils)
  #:use-module (guix build gnu-build-system)
  #:use-module (gnu packages image)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages tbb)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix gexp)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages version-control)
	#:use-module (gnu packages commencement)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
	#:use-module (gnu packages multiprecision)
	#:use-module (gnu packages python)
	#:use-module (gnu packages simulation)
	#:use-module (gnu packages boost)
	#:use-module (gnu packages maths)
	#:use-module (gnu packages python-xyz)
	#:use-module (gnu packages python-science)
	#:use-module (gnu packages graphics)
	#:use-module (gnu packages fontutils)
	#:use-module (gnu packages xml)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages base)
	#:use-module (guix build-system pyproject)
	#:use-module (guix build-system cmake)
	#:use-module (gnu packages lua)
	#:use-module (gnu packages tcl)
  #:use-module (gnu packages compression)
	#:use-module (gnu packages swig)
	#:use-module (gnu packages image-processing)
  #:use-module ((guix licenses) #:prefix license:))

(define-public python-decouple
(package
  (name "python-decouple")
  (version "0.0.7")
  (source
   (origin
     (method url-fetch)
     (uri (pypi-uri "decouple" version))
     (sha256
      (base32 "02bvhqdxcqnvdm2xyfxwhhcdxij84mak9wv6l0h9fvmpcb6c2ly2"))))
  (build-system pyproject-build-system)
  (home-page "https://github.com/andrey-avdeev/decouple")
  (synopsis "Decoupling logic")
  (description "Decoupling logic")
  (license license:asl2.0)))

(define-public simpleitkfilters-src
 (let ((commit "bb896868fc6480835495d0da4356d5db009592a6")
        (revision "1"))
    (package
      (name "simpleitkfilters-src")
      (version (git-version "1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/InsightSoftwareConsortium/ITKSimpleITKFilters.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "13nys94wl4k77f89i8y1dm3y4pmgmw3rrc0la1rzl0vi9h1qixii"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("." "src"))))
    (home-page "https://github.com/InsightSoftwareConsortium/ITKSimpleITKFilters.git")
    (synopsis "Source for ITKSimpleITKFilters")
    (description "The main purpose of this package is to build ITK with this
                 external module")
    (license license:gpl3))))

(define-public insight-toolkit-5.3.0
  (package
    (name "insight-toolkit")
    (version "5.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/InsightSoftwareConsortium/ITK/"
                           "releases/download/v" version "/InsightToolkit-"
                           version ".tar.gz"))
       (sha256
        (base32 "10z3f5xjniw72i58ha1l246x82s4bhl4bsynwfypd3yw6c8lg92p"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f ; tests require network access and external data
           #:configure-flags #~'("-DITK_USE_GPU=ON"
                                 "-DITK_USE_SYSTEM_LIBRARIES=ON"
                                 "-DITK_USE_SYSTEM_GOOGLETEST=ON"
                                 "-DITK_BUILD_SHARED=ON"
                                 "-DGTEST_ROOT=gtest"
                                 "-DCMAKE_CXX_STANDARD=17"
                                 "-DModule_SimpleITKFilters:BOOL=ON")

           #:phases #~(modify-phases %standard-phases
                        (add-after 'unpack 'do-not-tune
                          (lambda _
                            (substitute* "CMake/ITKSetStandardCompilerFlags.cmake"
                              (("-mtune=native")
                               ""))))
                        (add-after 'do-not-tune 'copy-filters-src
                          (lambda* (#:key outputs inputs configure-flags #:allow-other-keys)
                            (copy-recursively (string-append (assoc-ref %build-inputs
                                              "simpleitkfilters-src") "/src")
                                              "./Modules/Remote/SimpleITKFilters")))
                        (add-after 'copy-filters-src 'patch-cmake
                           (lambda* (#:key outputs inputs configure-flags #:allow-other-keys)
                             (substitute* "CMake/ITKModuleRemote.cmake"
                                          (("_git_update\\(") "#_git_update\\(")))
                                          ))))
    (inputs
     (list eigen
           expat
           fftw
           fftwf
           hdf5
           libjpeg-turbo
           libpng
           libtiff
           mesa-opencl
           perl
           python
           tbb
           vxl-1
           zlib
           ))
    (native-inputs
     (list googletest pkg-config simpleitkfilters-src git))

    ;; The 'CMake/ITKSetStandardCompilerFlags.cmake' file normally sets
    ;; '-mtune=native -march=corei7', suggesting there's something to be
    ;; gained from CPU-specific optimizations.
    (properties '((tunable? . #t)))

    (home-page "https://github.com/InsightSoftwareConsortium/ITK/")
    (synopsis "Scientific image processing, segmentation and registration")
    (description "The Insight Toolkit (ITK) is a toolkit for N-dimensional
scientific image processing, segmentation, and registration.  Segmentation is
the process of identifying and classifying data found in a digitally sampled
representation.  Typically the sampled representation is an image acquired
from such medical instrumentation as CT or MRI scanners.  Registration is the
task of aligning or developing correspondences between data.  For example, in
the medical environment, a CT scan may be aligned with a MRI scan in order to
combine the information contained in both.")
    (license license:asl2.0)))

(define-public python-simpleitk
(package
  (name "python-simpleitk")
  (version "2.3.1")
  (source 
   (origin
     (method url-fetch)
     (uri (string-append "https://github.com/SimpleITK/SimpleITK/releases/download/v" version  "/SimpleITK-" version  ".tar.gz"))
     (sha256 
      (base32 "0w2qf7267lab6jghp70059pxlvmi8qizvkkvq4kq08g87ayw363l"))))
  (build-system python-build-system)
  (arguments
    `(#:phases
        (modify-phases %standard-phases
          (delete 'check)
          (delete 'sanity-check)
          (add-before 'build 'build-simpleitk
            (lambda* (#:key outputs inputs configure-flags #:allow-other-keys)
             (mkdir-p "SimpleITK-build")
	           (chdir "SimpleITK-build")
	           (apply invoke "cmake" ".." (list "-DBUILD_EXAMPLES=OFF"
                                            "-DSimpleITK_FORBID_DOWNLOADS=ON"
                                            "-DCMAKE_VERBOSE_MAKEFILE=TRUE"
                                            "-DSimpleITK_PYTHON_USE_VIRTUALENV=OFF"
                                            "-DCMAKE_BUILD_TYPE=MinSizeRel"
                                            ;"-DBUILD_SHARED_LIBS=OFF"
                                            (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs
                                                                                           "out"))))
           (invoke "make")
           (chdir "Wrapping/Python")))))) 
  (inputs 
    (list hdf5 lua swig cmake gcc-toolchain insight-toolkit-5.3.0
         python-numpy git))
  (home-page "https://simpleitk.readthedocs.io/en/master/index.html")
  (synopsis "SimpleITK is a simplified, open source, interface to the Insight Toolkit (ITK), a C++ open source image analysis toolkit which is widely used in academia and industry.")
  (description "SimpleITK is a simplified, open source, interface to the Insight Toolkit (ITK), a C++ open source image analysis toolkit which is widely used in academia and industry. SimpleITK is available for eight programming languages including C++, Python, R, Java, C#, Lua, Ruby, and TCL.")  
  (license license:asl2.0)))

(define-public python-spam
 (package
  (name "python-spam")
  (version "0.6.5.1")
  (source (origin
            (method url-fetch)
            (uri (string-append "https://gitlab.com/spam-project/spam/-/archive/version-" version "/spam-version-" version ".tar.gz"))
            (sha256
             (base32
              "02qfa0whlzzq64cxvqbwcyr8fw961h50gj7srnbydm41ffz4kdhs"))))
  (build-system python-build-system)
  (arguments
    `(#:phases
       (modify-phases %standard-phases
        (delete 'check)
        (delete 'sanity-check)
        (add-after 'unpack 'post-unpack
          (lambda* (#:key outputs inputs #:allow-other-keys)
            (substitute* "setup.py"
              (("/usr/include/eigen3")
               (string-append (assoc-ref inputs "eigen") "/include/eigen3"))))))))
  (synopsis "spam is an open-source python tool for quantitative data analysis")
  (inputs 
    (list gcc-toolchain boost eigen cgal pybind11 gmp mpfr libxml2 freetype libxslt))
  (propagated-inputs
    (list python python-matplotlib python-numpy python-scipy
          python-progressbar33 python-meshio python-h5py python-scikit-image
          python-numba python-pyyaml python-tifffile python-decouple
          python-pygmsh python-simpleitk))
  (description
   "spam is an open-source python tool for quantitative data analysis for 2D images and 3D volumes for mechanics applications that has developed around x-ray and neutron tomography.")
  (home-page "https://www.spam-project.dev/")
  (license license:gpl3+)))
